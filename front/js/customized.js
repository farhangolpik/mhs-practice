console.log("* customized.js exist *");

/*
var findSwiper = $('body').find('div');

if( findSwiper.hasClass(".swiper-container") ){ // .hasClass() returns BOOLEAN true/false

  		var swiper = new Swiper('.swiper-container', {
            pagination: '.swiper-pagination',
                    slidesPerView: 3,
                    slidesPerColumn: 2,
                    paginationClickable: true,
                    spaceBetween: 30,
					autoplay: 2500,
					autoplayDisableOnInteraction: false,
					breakpoints: {
						1024: {	slidesPerView: 3, spaceBetween: 40 },
						768: { slidesPerView: 3, spaceBetween: 30 },
						640: { slidesPerView: 1, spaceBetween: 20 },
						320: { slidesPerView: 1, spaceBetween: 10 }
					}			
            });

}else{
	console.log("--> Swiper didn't found");
}

*/

				var swiper1 = new Swiper('.s1', {
							pagination: '.swiper-pagination',
							slidesPerView: '4',
							centeredSlides: false,
							paginationClickable: true,
							nextButton: '.swiper-button-next1',
							prevButton: '.swiper-button-prev1',
							spaceBetween: 15,
							autoplay: 2500,
							autoplayDisableOnInteraction: false,
							breakpoints: {
							1024: {	slidesPerView: 3, spaceBetween: 40 },
							768: { slidesPerView: 3, spaceBetween: 30 },
							640: { slidesPerView: 1, spaceBetween: 20 },
							320: { slidesPerView: 1, spaceBetween: 10 }
						}
					});
					
					
					var swiper2 = new Swiper('.s2', {
							pagination: '.swiper-pagination',
							slidesPerView: '4',
							centeredSlides: false,
							paginationClickable: true,
							nextButton: '.swiper-button-next2',
							prevButton: '.swiper-button-prev2',
							spaceBetween: 15,
							autoplay: 2500,
							autoplayDisableOnInteraction: false,
							breakpoints: {
							1024: {	slidesPerView: 3, spaceBetween: 40 },
							768: { slidesPerView: 3, spaceBetween: 30 },
							640: { slidesPerView: 1, spaceBetween: 20 },
							320: { slidesPerView: 1, spaceBetween: 10 }
						}
					});
					
					


jQuery(function () {
	jQuery('.slider--pauseplay').carousel({
		interval:2000,
		pause: "false"
	});
		jQuery('#playButton').click(function () {
			jQuery("#carouselButtons button").removeClass("is-active");
			jQuery('.slider--pauseplay').carousel('cycle');
			jQuery(this).addClass("is-active");
		});
			jQuery('#pauseButton').click(function () {
				jQuery("#carouselButtons button").removeClass("is-active");
				jQuery('.slider--pauseplay').carousel('pause');
				jQuery(this).addClass("is-active");
			});
		});


		
		jQuery( "#girdBoxThumbStyle" ).click(function() {
						jQuery(".gridbox-icons button").removeClass("is-active");
						jQuery(".girdbox-area").removeClass("girdbox-list");
						jQuery(".girdbox-area").addClass("girdbox-thumbnail");
						jQuery(this).addClass("is-active");
					});
					jQuery( "#girdBoxListStyle" ).click(function() {
						jQuery(".gridbox-icons button").removeClass("is-active");
						jQuery(".girdbox-area").removeClass("girdbox-thumbnail");
						jQuery(".girdbox-area").addClass("girdbox-list");
						jQuery(this).addClass("is-active");
					});
					

					
					
					var btnClicked = "btn-loading";
var btnName = "button";
jQuery(btnName).on('click',function(e){
    jQuery(this).addClass(btnClicked);
   jQuery(this).prop("disabled",true);
   jQuery(this).append('<span class="btn-tmploading"><i  class="fa fa-spin fa-refresh"></i></span>');
   setTimeout(function(g){
         jQuery(btnName).removeClass(btnClicked);
         jQuery(btnName+" .btn-tmploading").remove();
         jQuery(btnName).prop("disabled", false);
    }, 4000);
  });