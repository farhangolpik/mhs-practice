<?php include("header.php"); ?>

	<section class="slider-area hover-ctrl fadeft " >
		<div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
		
		  <ol class="carousel-indicators thumbs">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">


			<div class="item active bg-cntr" style="background-image:url('images/slide1.jpg');">
			  
			  <div class="caro-cap col-sm-5">
				  
				  	<h3>Trust Worthy & Timely</h3>
					<h1>HEALTH & <span>MEDICAL</span></h1>
					<div class="cont">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</div>
					<a class="lnk-btn btn btn-primary">READ MORE...</a>
				  
			  </div>
			  
			</div>
			
			
			<div class="item bg-cntr" style="background-image:url('images/slide1.jpg');">
			  
			  <div class="caro-cap col-sm-5">
				  
				  	<h3>Trust Worthy & Timely</h3>
					<h1>HEALTH & <span>MEDICAL</span></h1>
					<div class="cont">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</div>
					<a class="lnk-btn btn btn-primary">READ MORE...</a>
				  
			  </div>
			  
			</div>
			
			<div class="item bg-cntr" style="background-image:url('images/slide1.jpg');">
			  
			  <div class="caro-cap col-sm-5">
				  
				  	<h3>Trust Worthy & Timely</h3>
					<h1>HEALTH & <span>MEDICAL</span></h1>
					<div class="cont">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</div>
					<a class="lnk-btn btn btn-primary btn-wide">READ MORE...</a>
				  
			  </div>
			  
			</div>
			
			
			
		  </div>

		  <a class="left carousel-control hidden" href="#carousel-example-generic" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control hidden" href="#carousel-example-generic" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		  
		   <div id="carouselButtons" class="pul-rgt">
				  <button id="playButton" type="button" class="btn btn-default btn-xs is-active">
					  <span class="glyphicon glyphicon-play"></span>
				   </button>
				  <button id="pauseButton" type="button" class="btn btn-default btn-xs">
					  <span class="glyphicon glyphicon-pause"></span>
				  </button>
 			</div>
			
		  
		</div>
		
		
	</section>
	
	
	
	
	
	<section class="srv-area bg-cvr dark-area" style="background-image:url('images/srv-bg.png')">
		<div class="container">
			
			<div class="hed underline--plus">
				<h2>SERVICES</h2>
			</div>
			
			
			<div class="srvtabs-area tabs-change--hover">
				<ul class="nav nav-tabs">
				  <li>
					  <a href="#srvtab1" data-toggle="tab">
						<i class="icon"><img src="images/srvicon1.png" alt="" /></i>
						<span>DRUGS & COMMODITIES</span>				  
					  </a>
				  </li>
				  <li>
					  <a href="#srvtab2" data-toggle="tab">
						<i class="icon"><img src="images/srvicon2.png" alt="" /></i>
						<span>REVERSE LOGISTICS</span>				  
					  </a>
				  </li>
				  <li>
					  <a href="#srvtab3" data-toggle="tab">
						<i class="icon"><img src="images/srvicon3.png" alt="" /></i>
						<span>HEALTH ADVISORY</span>				  
					  </a>
				  </li>
				 <!-- <li>
					  <a href="#srvtab4" data-toggle="tab">
						<i class="icon"><img src="images/srvicon4.png" alt="" /></i>
						<span>ENDOCRINOLOGY</span>				  
					  </a>
				  </li> -->
				</ul> 
				
				<div class="tab-content overload">
				  <div class="tab-pane active" id="srvtab1">
					
					<div class="srvtab__cont col-sm-7 cont list-icon--arr clrlist listview">
						<h4>DRUGS & COMMODITIES</h4>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley .</p>
						
						<ul>
							<li>Lorem Ipsum is simply dummy text of the printing.</li>
							<li>Typesetting industry. Lorem Ipsum has been the industry's standard.</li>
							<li>Dummy text ever since the 1500s, when an unknown printer took a galley.</li>
						</ul>
					</div>
					<div class="srvtab__img col-sm-5 img">
						<div class="__img">
							<img src="images/tabpic1.jpg" alt="" />
						</div>
					</div>
						
				  
				  </div>
				  <div class="tab-pane" id="srvtab2">
				  
				  
					<div class="srvtab__cont col-sm-7 cont list-icon--arr clrlist listview">
						<h4>REVERSE LOGISTICS</h4>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley .</p>
						
						<ul>
							<li>Lorem Ipsum is simply dummy text of the printing.</li>
							<li>Typesetting industry. Lorem Ipsum has been the industry's standard.</li>
							<li>Dummy text ever since the 1500s, when an unknown printer took a galley.</li>
						</ul>
					</div>
					<div class="srvtab__img col-sm-5 img">
						<div class="__img">
							<img src="images/tabpic1.jpg" alt="" />
						</div>
					</div>
					
				  </div>
				  <div class="tab-pane" id="srvtab3">
				  
					<div class="srvtab__cont col-sm-7 cont list-icon--arr clrlist listview">
						<h4>HEALTH ADVISORY</h4>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley .</p>
						
						<ul>
							<li>Lorem Ipsum is simply dummy text of the printing.</li>
							<li>Typesetting industry. Lorem Ipsum has been the industry's standard.</li>
							<li>Dummy text ever since the 1500s, when an unknown printer took a galley.</li>
						</ul>
					</div>
					<div class="srvtab__img col-sm-5 img">
						<div class="__img">
							<img src="images/tabpic1.jpg" alt="" />
						</div>
					</div>
					
					</div>
					
			<!--	  <div class="tab-pane" id="srvtab4">
				  
					<div class="srvtab__cont col-sm-7 cont list-icon--arr clrlist listview">
						<h4>ENDOCRINOLOGY</h4>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley .</p>
						
						<ul>
							<li>Lorem Ipsum is simply dummy text of the printing.</li>
							<li>Typesetting industry. Lorem Ipsum has been the industry's standard.</li>
							<li>Dummy text ever since the 1500s, when an unknown printer took a galley.</li>
						</ul>
					</div>
					<div class="srvtab__img col-sm-5 img">
						<div class="__img">
							<img src="images/tabpic1.jpg" alt="" />
						</div>
					</div>
					
				  </div> -->
				  
				</div>
			</div>
			
		</div>
	</section>
	
	
	
	
	<section class="prod-area bg-cvr" style="background-image:url('images/prod-bg.jpg')">
		<div class="container">
			
			<div class="hed underline--plus">
				<h2>MEDICAL PRODUCTS</h2>
			</div>
			
			<div class="prod__seachbar form-group">
				<div class="input-group">
					<select class="form-control">
						<option>- Select  Product Type -</option>
						<option>- Select  Product Type -</option>
						<option>- Select  Product Type -</option>
						<option>- Select  Product Type -</option>
						<option>- Select  Product Type -</option>
					</select>
					<span class="input-group-addon w30">
						<button>FIND PRODUCT</button>
					</span>
				</div>
				
			</div>
			
			
			<div class="prod-main col-sm-11-5 pul-cntr">
			<h3>CONSUMABLES</h3>
			<div class="prod-left prod-sideimg col-sm-4 p0">
				<img src="images/prod-left.jpg" alt="" />			
			</div>
			
				<div class="prod-right prod-group col-sm-8 bg-white p0">
					
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							<div class="prodbox__cont">
								<h4>Syringes</h4>
								<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
							</div>
							<div class="prodbox__img">
								<img src="images/prod1.jpg" alt="" />
							</div>
							<div class="prodbox__actions clrlist listview">
								<ul>
									<li><a href="#"><i class="fa fa-heart"></i></a></li>
									<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="#"><i class="fa fa-search"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							<div class="prodbox__cont">
								<h4>Needles</h4>
								<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
							</div>
							<div class="prodbox__img">
								<img src="images/prod2.jpg" alt="" />
							</div>
							
							<div class="prodbox__actions clrlist listview">
								<ul>
									<li><a href="#"><i class="fa fa-heart"></i></a></li>
									<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="#"><i class="fa fa-search"></i></a></li>
								</ul>
							</div>
							
						</div>
					</div>
					
					
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							<div class="prodbox__cont">
								<h4>Intravenous Fluids</h4>
								<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
							</div>
							<div class="prodbox__img">
								<img src="images/prod3.jpg" alt="" />
							</div>
							
							<div class="prodbox__actions clrlist listview">
								<ul>
									<li><a href="#"><i class="fa fa-heart"></i></a></li>
									<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="#"><i class="fa fa-search"></i></a></li>
								</ul>
							</div>
							
						</div>
					</div>
					
					
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							<div class="prodbox__cont">
								<h4>Sterile Cotton</h4>
								<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
							</div>
							<div class="prodbox__img">
								<img src="images/prod4.jpg" alt="" />
							</div>
							
							<div class="prodbox__actions clrlist listview">
								<ul>
									<li><a href="#"><i class="fa fa-heart"></i></a></li>
									<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="#"><i class="fa fa-search"></i></a></li>
								</ul>
							</div>
							
						</div>
					</div>
					
					
					
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							<div class="prodbox__cont">
								<h4>Antiseptics</h4>
								<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
							</div>
							<div class="prodbox__img">
								<img src="images/prod5.jpg" alt="" />
							</div>
							
							
							<div class="prodbox__actions clrlist listview">
								<ul>
									<li><a href="#"><i class="fa fa-heart"></i></a></li>
									<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="#"><i class="fa fa-search"></i></a></li>
								</ul>
							</div>
							
						</div>
					</div>
					
					
					
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							<div class="prodbox__cont">
								<h4>Injections Water</h4>
								<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
							</div>
							<div class="prodbox__img">
								<img src="images/prod6.jpg" alt="" />
							</div>
							
							
							<div class="prodbox__actions clrlist listview">
								<ul>
									<li><a href="#"><i class="fa fa-heart"></i></a></li>
									<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="#"><i class="fa fa-search"></i></a></li>
								</ul>
							</div>
							
						</div>
					</div>
					
				
				</div>
			</div>
			
		</div>
	</section>
	
	<section class="homecontact-area bg-cvr dark-area" style="background-image:url('images/contact-bg.png')">
		<div class="container">
			
		
			<div class="hed underline--plus">
				<h2>GET IN TOUCH WITH US</h2>
			</div>
			
			
			<div class="fom fom-home row">
			
				<div class="form-group col-sm-6">
					<input class="form-control" placeholder="First Name" />
				</div>
				
				<div class="form-group col-sm-6">
					<input class="form-control" placeholder="Last Name" />
				</div>
				
				<div class="form-group col-sm-6">
					<input class="form-control" placeholder="Email" />
				</div>
				
				<div class="form-group col-sm-6">
					<input class="form-control" placeholder="Phone Number" />
				</div>
				
				<div class="form-group col-sm-12">
					<textarea class="form-control mh" placeholder="Message Here!" rows="4" ></textarea>
				</div>
				
				<div class="btns col-sm-12 text-center">
					<button class="btn btn-default btn-wide">SEND</button>
				</div>
			
			
			</div>
			
		</div>
	</section>
			

<?php include("footer.php"); ?>