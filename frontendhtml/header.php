<!DOCTYPE html>
<html lang="en" class="broken-image-checker">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!--iPhone from zooming form issue (maximum-scale=1, user-scalable=0)-->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title>Home</title>

	<link rel="icon" type="image/png" href="images/favicon.png">
	
    <!-- Bootstrap --><link href="css/bootstrap.min.css" rel="stylesheet">
	
	<link rel="stylesheet" href="css/stylized.css">
	<link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="style-extra.css">
	<link rel="stylesheet" href="css/colorized.css">
	<link rel="stylesheet" href="css/animate.css">
	<link rel="stylesheet" href="css/slidenav.css">
	<link rel="stylesheet" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="css/swiper.min.css">
	
	<!-- jQuery -->
	<!--[if (!IE)|(gt IE 8)]><!-->
	  <script src="js/jquery-2.2.4.min.js"></script>
	<!--<![endif]-->


	<!--[if lte IE 8]>
	  <script src="js/jquery1.9.1.min.js"></script>
	<![endif]-->
	
	<!--browser selector-->
	<script src="js/css_browser_selector.js" type="text/javascript"></script>
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	

  </head>
  <body class="transition">

  <div id="body-wrapper" class="container">

	<header>
	
		<section class="hdr-top bdr-grid bg-white">
			<div class="container0">
				
				<div class="top__contactinfo col-sm-7 clrlist bg-white p0">
					<ul>
						<li><a href="#"><i class="fa fa-phone"></i> <span>+234 (0) 90 99 60 1329</span></a></li>
						<li><a href="#"><i class="fa fa-map-marker"></i> <span>Abuja Office, 12 Sheik Ismail Street</span></a></li>
						<li><a href="#"><i class="fa fa-clock-o"></i> <span>MON - SAT : 8 A.M - 9.00 P.M</span></a></li>
						
					</ul>
				</div>
				
				
				<div class="top__social col-sm-5 clrlist text-right bg-white p0">
				
					<ul>
						<li class="shopping"><a href="cart.php"><i class="fa fa-shopping-cart"></i><span class="badge">0</span></a></li>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					</ul>
				</div>
				
			</div>
		</section>
	
	
	
	<section class="hdr-area hdr-nav  cross-toggle navbar-overide">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation" id="slide-nav">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<a class="navbar-brand" href="./"><img src="images/logo.png" alt="logo" class="broken-image"/></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div id="slidemenu">
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					  
					  
					  <a class="btn btn-primary btn-login fr" href="login.php">LOGIN</a>
					  
					  <ul class="nav navbar-nav navbar-main fr">
					  
						<li><a href="index.php#!5">Home</a></li>
						 
						<li class="dropdown submenu-cols">
						  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Products</a>
						  <ul class="dropdown-menu">
							<li class="dropdown">
							  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">General Drugs</a>
							  <ul class="dropdown-menu">
								<li><a href="products.php">Action</a></li>
								<li><a href="products.php">Another action</a></li>
								<li><a href="products.php">Something else here</a></li>
							  </ul>
							</li>
							<li class="dropdown">
							  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Specialty Pulmonology</a>
							  <ul class="dropdown-menu">
								<li><a href="products.php">Action</a></li>
								<li><a href="products.php">Another action</a></li>
								<li><a href="products.php">Something else here</a></li>
							  </ul>
							</li>
							<li class="dropdown">
							  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Specialty Dermatology</a>
							  <ul class="dropdown-menu">
								<li><a href="products.php">Action</a></li>
								<li><a href="products.php">Another action</a></li>
								<li><a href="products.php">Something else here</a></li>
							  </ul>
							</li>
							<li class="dropdown">
							  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Anti-Infectives</a>
							  <ul class="dropdown-menu">
								<li><a href="products.php">Action</a></li>
								<li><a href="products.php">Another action</a></li>
								<li><a href="products.php">Something else here</a></li>
							  </ul>
							</li>
							<li class="dropdown">
							  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Specialty Oncology</a>
							  <ul class="dropdown-menu">
								<li><a href="products.php">Action</a></li>
								<li><a href="products.php">Another action</a></li>
								<li><a href="products.php">Something else here</a></li>
							  </ul>
							</li>
							<li class="dropdown">
							  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Specialty Psychiatry</a>
							  <ul class="dropdown-menu">
								<li><a href="products.php">Action</a></li>
								<li><a href="products.php">Another action</a></li>
								<li><a href="products.php">Something else here</a></li>
							  </ul>
							</li>
							
							<li class="dropdown">
							  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Specialty Cardiovascular</a>
							  <ul class="dropdown-menu">
								<li><a href="products.php">Action</a></li>
								<li><a href="products.php">Another action</a></li>
								<li><a href="products.php">Something else here</a></li>
							  </ul>
							</li>
							
							
							<li class="dropdown">
							  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Specialty Nephrology</a>
							  <ul class="dropdown-menu">
								<li><a href="products.php">Action</a></li>
								<li><a href="products.php">Another action</a></li>
								<li><a href="products.php">Something else here</a></li>
							  </ul>
							</li>
							
							
							<li class="dropdown">
							  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Anesthesia</a>
							  <ul class="dropdown-menu">
								<li><a href="products.php">Action</a></li>
								<li><a href="products.php">Another action</a></li>
								<li><a href="products.php">Something else here</a></li>
							  </ul>
							</li>
							
							
							<li class="dropdown">
							  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Specialty Endocrinology</a>
							  <ul class="dropdown-menu">
								<li><a href="products.php">Action</a></li>
								<li><a href="products.php">Another action</a></li>
								<li><a href="products.php">Something else here</a></li>
							  </ul>
							</li>
							
							
							<li class="dropdown">
							  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Specialty Rheumatology</a>
							  <ul class="dropdown-menu">
								<li><a href="products.php">Action</a></li>
								<li><a href="products.php">Another action</a></li>
								<li><a href="products.php">Something else here</a></li>
							  </ul>
							</li>
							
							
							<li class="dropdown">
							  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Biosimilars</a>
							  <ul class="dropdown-menu">
								<li><a href="products.php">Action</a></li>
								<li><a href="products.php">Another action</a></li>
								<li><a href="products.php">Something else here</a></li>
							  </ul>
							</li>
							
							
						  </ul>
						</li>
						 
						<li class="dropdown">
						  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Laboratory Services</a>
						  <ul class="dropdown-menu">
							<li><a href="#c">Action</a></li>
							<li><a href="#c">Another action</a></li>
							<li><a href="#c">Something else here</a></li>
						  </ul>
						</li>
						
						 
						<li class="dropdown">
						  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Services</a>
						  <ul class="dropdown-menu">
							<li><a href="#c">Reverse Logistics</a></li>
							<li><a href="#f">Health Professional Services</a></li>
							<li><a href="#e">Strategic Health Analysis and Advisory Services</a></li>
						  </ul>
						</li>
						
						<li><a href="#!5">Contact us</a></li>
						
					  </ul>
					  
					  
						

					</div><!-- /.navbar-collapse -->
				</div>
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</section>
</header>

 <main id="page-content">
