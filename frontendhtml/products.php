<?php include("header.php"); ?>


<section class="page-banner col-sm-12 bg-cvr dark-area" style="background-image:url('images/product-bg.jpg');">
	
	<div class="hed underline--plus">
		<h2>PRODUCTS</h2>
	</div>
	
	<ol class="breadcrumb">
	  <li><a href="#">Home</a></li>
	  <li class="active">Products</li>
	</ol>
	
</section>



<section class="products-page last-section bg-white">

	<div class="prod__left col-sm-3 ">
					
					<h3>Medical Products</h3>
				
					<nav class="navbar navbar-default" role="navigation">
					  <div class="container-fluid p0">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						</div>

						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse p0" id="bs-example-navbar-collapse-1">
						
						  <ul class="nav navbar-nav navbar-list navbar-mega">
						  
							<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">General Drugs</a>
							  
									  <ul class="dropdown-menu">
										<li><a href="#">Analgesics</a></li>
										<li><a href="#">Antitussives</a></li>
										<li><a href="#">Anti-diarrheals</a></li>
									  </ul>
								  
							</li>
							
			

							<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Anti-Infectives</a>
							  <ul class="dropdown-menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Political</a></li>
								<li><a href="#">Business</a></li>
								<li><a href="#">Sports</a></li>
							  </ul>
							</li>
							
							
							<li><a href="#">Specialty Cardiovascular</a></li>
							
							<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Specialty Endocrinology</a>
							  <ul class="dropdown-menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Political</a></li>
								<li><a href="#">Business</a></li>
								<li><a href="#">Sports</a></li>
							  </ul>
							</li>
							
							<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Specialty Pulmonology</a>
							  <ul class="dropdown-menu">
								<li><a href="#">Action</a></li>
								<li><a href="#">Another action</a></li>
								<li><a href="#">Political</a></li>
								<li><a href="#">Business</a></li>
								<li><a href="#">Sports</a></li>
							  </ul>
							</li> 
							
							<li><a href="#">Specialty Oncology</a></li>
							<li><a href="#">Specialty Nephrology</a></li>
							<li><a href="#">Specialty Rheumatology</a></li>
							<li><a href="#">Specialty Dermatology</a></li>
							<li><a href="#">Specialty Psychiatry</a></li>
							<li><a href="#">Anesthesia</a></li>
							<li><a href="#">Biologics and Biosimilars</a></li>
							 
							
						  </ul>
						  
						</div><!-- /.navbar-collapse -->
					  </div><!-- /.container-fluid -->
					</nav>
				</div>

				
	<div class="prod__right col-sm-9 girdbox-area ">		
			
			
			<div class="sorting-bar col-sm-12 p0 global-active mb20">
			
				<div class="gridbox-icons icon2x col-sm-3" id="girdBox">
					<button id="girdBoxThumbStyle"><i class="glyphicon glyphicon-th-large"></i></button>
					<button id="girdBoxListStyle"><i class="glyphicon glyphicon-th-list"></i></button>
				</div>
				
				<div class="sort col-sm-5">
					<div class="input-group">
					<span class="input-group-addon">Sort By</span>
					<select class="form-control">
						<option>Position</option>
						<option>Position</option>
						<option>Position</option>
					</select>
					</div>
				</div>
				
				
				<div class="pagi-area col-sm-4 text-right">
					<ul class="pagination">
					  <li class="active"><a href="#">1</a></li>
					  <li><a href="#">2</a></li>
					  <li><a href="#">3</a></li>
					  <li><a href="#">&raquo;</a></li>
					</ul>
				</div>
			</div>

		
		<div class="row">
		
			<div class="prod-group">
			
			
			
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							
							<div class="prodbox__img">
								<img src="images/product1.jpg" alt="" />
							</div>
							
							<div class="prodbox__cont">
								<h4>Cotrapper Tab</h4>
								<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
								<div class="prod__price">
									<span>₦</span> 15.00
								</div>
								
								<div class="prod__atc">
									<a href="products-view.php" class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</a>							
								</div>
							</div>
							
							<div class="prodbox__actions clrlist listview">
								<ul>
									<li><a href="#"><i class="fa fa-heart"></i></a></li>
									<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="#"><i class="fa fa-search"></i></a></li>
								</ul>
							</div>
							
						</div>
					</div>
					
					
					
			
			
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							
							<div class="prodbox__img">
								<img src="images/product2.jpg" alt="" />
							</div>
							<div class="prodbox__cont">
								<h4>DEFEND Tab</h4>
								<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
								<div class="prod__price">
									<span>₦</span> 10.00
								</div>
								
								<div class="prod__atc">
									<button class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</button>	
								</div>
								
							</div>
							
							<div class="prodbox__actions clrlist listview">
								<ul>
									<li><a href="#"><i class="fa fa-heart"></i></a></li>
									<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="#"><i class="fa fa-search"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					
			
			
			
			
			
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							
							<div class="prodbox__img">
								<img src="images/product3.jpg" alt="" />
							</div>
							<div class="prodbox__cont">
								<h4>Nexium Tab</h4>
								<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
								<div class="prod__price">
									<span>₦</span> 35.00
								</div>
								
								<div class="prod__atc">
									<button class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</button>							
								</div>
								
							</div>
							
							<div class="prodbox__actions clrlist listview">
								<ul>
									<li><a href="#"><i class="fa fa-heart"></i></a></li>
									<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="#"><i class="fa fa-search"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					
					
					
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							
							<div class="prodbox__img">
								<img src="images/product4.jpg" alt="" />
							</div>
							<div class="prodbox__cont">
								<h4>c 500 Tab</h4>
								<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
								<div class="prod__price">
									<span>₦</span> 80.00
								</div>
								
								<div class="prod__atc">
									<button class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</button>							
								</div>
								
							</div>
							
							<div class="prodbox__actions clrlist listview">
								<ul>
									<li><a href="#"><i class="fa fa-heart"></i></a></li>
									<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="#"><i class="fa fa-search"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					
			
			
			
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							
							<div class="prodbox__img">
								<img src="images/product5.jpg" alt="" />
							</div>
							<div class="prodbox__cont">
								<h4>Migrelief Tab</h4>
								<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
								<div class="prod__price">
									<span>₦</span> 22.00
								</div>
								
								<div class="prod__atc">
									<button class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</button>							
								</div>
								
							</div>
							
							<div class="prodbox__actions clrlist listview">
								<ul>
									<li><a href="#"><i class="fa fa-heart"></i></a></li>
									<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="#"><i class="fa fa-search"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					
					
			
			
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							
							<div class="prodbox__img">
								<img src="images/product5.jpg" alt="" />
							</div>
							<div class="prodbox__cont">
								<h4>Selter Tab</h4>
								<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
								<div class="prod__price">
									<span>₦</span> 95.00
								</div>
								
								<div class="prod__atc">
									<button class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</button>							
								</div>
								
							</div>
							
							<div class="prodbox__actions clrlist listview">
								<ul>
									<li><a href="#"><i class="fa fa-heart"></i></a></li>
									<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="#"><i class="fa fa-search"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					
					
					
			
			</div>
				
		</div>
	</div>
				
				
				<?php
				
				include("related-products.php");
				
				
				?>
				
</section>
<?php include("footer.php"); ?>