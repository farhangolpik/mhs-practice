<?php include("header.php"); ?>

<section class="page-banner col-sm-12 bg-cvr dark-area" style="background-image:url('images/product-bg.jpg');">
	
	<div class="hed underline--plus">
		<h2>LOGIN</h2>
	</div>
	
	<ol class="breadcrumb">
	  <li><a href="#">Home</a></li>
	  <li class="active">Login</li>
	</ol>
	
</section>

	<section class="login-section col-sm-12 pt60">
		  
		  <div class="login-form fom col-sm-8 col-sm-offset-2 placeholder--hover pr50 pl50 overload">
			
			<div class="hed lv2 mb30">
				<h2>Login</h2>
			</div>
			  <form class="login__form">
			
				<div class="form-group">
					 <input type="text" class="form-control" name="username" placeholder="Username*" required/>
		        </div>
				<div class="form-group">
		            <input type="text" class="form-control" name="password" placeholder="Password*" required/>
		        </div>
				<div class="form-group">
					 <input type="checkbox" class="form-control"/>Remember me</label>
				 </div>
				<div class="login-btn">
					<button class="btn btn-primary w100">Login</button>
		        </div>
				
			  </form>
			  <div class="col-sm-12 fpwd mt20 mb40 text-center">
					<a href="">Lost your password?</a>
			  </div>
			  <div class="clearfix"></div>
			
		   </div>
		   
	</section>


<?php include("footer.php"); ?>