<?php include("header.php"); ?>


<section class="page-banner col-sm-12 bg-cvr dark-area" style="background-image:url('images/product-bg.jpg');">
	
	<div class="hed underline--plus">
		<h2>PRODUCTS</h2>
	</div>
	
	<ol class="breadcrumb">
	  <li><a href="#">Home</a></li>
	  <li><a href="#">General Drugs</a></li>
	  <li class="active">Analgesics</li>
	</ol>
	
</section>



<section class="products-page proddtl-area last-section bg-white">

	<div class="row">
	
	<div class="proddtl__img col-sm-6 thumb-indicat">
	
	<div id="prodSlider" class="carousel slide" data-ride="carousel">
	  <ol class="carousel-indicators">
		<li data-target="#prodSlider" data-slide-to="0" class="active">
			<img src="images/product3.jpg" alt="" />
		</li>
		<li data-target="#prodSlider" data-slide-to="1">
			<img src="images/product4.jpg" alt="" />
		</li>
		<li data-target="#prodSlider" data-slide-to="2">
			<img src="images/product5.jpg" alt="" />
		</li>
		<li data-target="#prodSlider" data-slide-to="3">
			<img src="images/product6.jpg" alt="" />
		</li>
	  </ol>

	  
	  <div class="carousel-inner">
		<div class="item bg-cntr active" style="background-image:url('images/prod-dtl1.png');">
		</div>
		<div class="item bg-cntr" style="background-image:url('images/prod-dtl1.png');">
		</div>
		<div class="item bg-cntr" style="background-image:url('images/prod-dtl1.png');">
		</div>
		<div class="item bg-cntr" style="background-image:url('images/prod-dtl1.png');">
		</div>
	  </div>
 
	  <a class="left carousel-control hidden" href="#prodSlider" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	  </a>
	  <a class="right carousel-control hidden" href="#prodSlider" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	  </a>
	</div>


	</div>
	
	
	<div class="proddtl__cont col-sm-6">
	
		<h3>NEXIUM TAB</h3>
		
		<div class="rating-area">
			
			<span class="star-icon star-4">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
            </span>
									
			<span>1 Review(s) | Write Your Review</span>
		</div>
		
		
		<div class="instock"><small>Availability</small><strong>(In stock)</strong></div>
		
		
		<div class="prod__price">
			<span>₦</span> 15.00
		</div>
		
		<div class="cont">
		It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.
		</div>
		
		
		<div class="proddtl__actions clrlist">
				
		<ul>
			<li>
				<div class="form-group input-number-area">
					<div class="input-group">
					<span class="input-group-addon qty">Qty</span>
						<input class="form-control input-number has-value" type="number" value="1" min="0" max="10">
						<span class="input-group-addon">
							<span class="form-control input-number-decrement">-</span>
							<span class="form-control input-number-increment">+</span>
							
						</span>
					</div>
				</div>
			</li>
			<li>
				<button class="btn btn-primary">Add to cart</button>
			</li>
			<li>
				<button class="btn"><i class="fa fa-heart"></i></button>
			</li>
			<li>
				<button class="btn"><i class="fa fa-bar-chart "></i></button>
			</li>
		</ul>
						
		</div>			
	
	</div>
	
	</div>
	
	<div class="clearfix"></div>
	
	<div class="proddtl__desc mt30">
		<ul class="nav nav-tabs">
		  <li class="active"><a href="#proddtltab1" data-toggle="tab">Details</a></li>
		  <li><a href="#proddtltab2" data-toggle="tab">More Information</a></li>
		  <li><a href="#proddtltab3" data-toggle="tab">Reviews</a></li>
		</ul>


		<div class="tab-content p15">
		  <div class="tab-pane active" id="proddtltab1">
		  It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.
		  </div>
		  <div class="tab-pane" id="proddtltab2">
		 look like readable English. have evolved over the years, sometimes by accident, sometimes on purp It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it ose (injected humour and the like).It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters.
		  </div>
		  <div class="tab-pane" id="proddtltab3">
		  ibution of letters, as opposed to using 'Content here, content here', making it look like readable English. have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).It is a long established fact that a reader will be distracted by the readable content of a page when looking at its</div>
		</div>
	</div>

				
	<?php
				
		include("related-products.php");
				
	?>
				
</section>
<?php include("footer.php"); ?>