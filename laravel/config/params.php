<?php
return [
    'site_name' => 'Medicare Health System',
    'order_email' => 'order@medicarehealthsystem.com.',
    'from_email' => 'from@medicarehealthsystem.com',
	'currency'=>[
            'USD'=>['symbol'=>'₦','name'=>'Nigerian Naira'],
    ],
    'currency_default'=>'USD',
    'languages'=>[
            'en_uk'=>'English (UK)',
            'en_us'=>'English (Us)',
    ],
    'language_default'=>'en_uk',
    'contentTypes'=>[
            'page'=>'Page',
            'email'=>'Email',
            'block'=>'Block',
    ],
    'order_prefix'=>"ncl",
    'password_pattern' => '(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}',
    'keywords' => 'test',
    'meta_description'=>'Traditionally to order a .'
    
];
?>
