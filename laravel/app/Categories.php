<?php namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model {

	//
	
    public static function getServiceCategories(){
        
        
        
        $sql="select c.* from products p JOIN products_categories pc on pc.product_id=p.id JOIN categories c ON c.id=pc.category_id where p.type='service' group by c.id";
        return DB::select($sql);
    }

}
