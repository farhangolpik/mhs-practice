<?php

namespace App\Functions;

use Mail;

class Functions {

    public static function prettyJson($inputArray, $statusCode) {
        return response()->json($inputArray, $statusCode, array('Content-Type' => 'application/json'), JSON_PRETTY_PRINT);
    }

    public static function saveImage($file, $destinationPath, $destinationPathThumb = '') {
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(111, 999) . time() . '.' . $extension;
        $image = $destinationPath . '/' . $fileName;
        $upload_success = $file->move($destinationPath, $fileName);
        //Functions::saveThumbImage($image,'fit',$destinationPath.$fileName);
        return $fileName;
    }

    // remove string from any length and concatinate three dots at end any string condition needle start to end 
    public static function stringTrim($string = '', $needle = 0, $start = 0, $end = 0) {
        return (strlen($string) > $needle) ? substr($string, $start, $end) . '...' : $string;
    }

    public static function makeOrderEmailTemplate($orders, $addresses) {
        //d($order,1);
        $template = "";

        return view('email.order', compact('orders', 'addresses'));
        //die('aaa');
    }

    public static function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function setEmailTemplate($contentModel, $replaces) {
        $data['body'] = $contentModel[0]->body;
        $data['subject'] = $contentModel[0]->subject;
        $data['title'] = $contentModel[0]->title;
        foreach ($replaces as $key => $replace) {
            $data['body'] = str_replace("%%" . $key . "%%", $replace, $data['body']);
        }

        return $data;
    }

    public static function sendEmail($email, $subject, $body, $header = '', $from = "customerservice@medicare.com", $cc = "", $bcc = "") {


        $data['to'] = $email;
        $data['body'] = $body;
        $data['subject'] = $subject;


        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers .= 'From: <' . $from . '>' . "\r\n";
        // $headers .= 'Cc: myboss@example.com' . "\r\n";
        //return mail($email, $subject, $body, $headers);


        return Mail::send('emails.template', $data, function($message) use ($data) {
                    $message->SMTPOptions = array('ssl' => array('verify_peer' => false,
                            'verify_peer_name' => false,
                            'allow_self_signed' => true
                        )
                    );
                    $message->to($data['to'])->subject($data['subject']);
                });



        // $body = "<title></title><style></style></head><body>" . $body . "</body></html>";
        // 
    }

    public static function makeCurlRequest($url) {
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
                )
        );
        $resp = curl_exec($curl);
        curl_close($curl);
        return $resp;
    }

    public static function setTemplate($body, $replaces) {

        $replaces["asset('')"] = asset("");
        $replaces["url('')"] = url("");
        foreach ($replaces as $key => $replace) {
            // $key=str_replace(" ", "", $key);
            $body = str_replace("{{" . $key . "}}", $replace, $body);
        }
        return $body;
    }

    public static function getCategories($allCategories) {
        $categories = array();

        foreach ($allCategories as $category) {
            if ($category->parent_id == 0) {
                $categories[$category->id]['name'] = $category->name;
            } else {
                $categories[$category->parent_id]['categories'][$category->id] = $category->name;
            }
        }
        return $categories;
    }

    public static function getPrice($user, $product) {

        $price = $product->price;
        if ($product->sale == 1 && $product->price > $product->salePrice) {
            $price = $product->salePrice;
        }

        if (isset($user->id)) {
            $role = \App\Role::find($user->role_id);
            if (strtolower(trim($role->role)) == 'doctor') {
                $price = $product->priceForDoctors;
            }
        }


        return $price;
    }

    function slugify($text) {
        // replace non letter or digits by -
        $text = preg_replace('~[^\pL\d]+~u', '-', $text);

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        // trim
        $text = trim($text, '-');

        // remove duplicate -
        $text = preg_replace('~-+~', '-', $text);

        // lowercase
        $text = strtolower($text);

        if (empty($text)) {
            return 'n-a';
        }

        return $text;
    }

	
	 public static function pagination($result_count, $per_page = 10,$page = 1, $url = '?',$url_prefix='/screen-'){        
        
        $total = $result_count;
        $adjacents = "2"; 
 
        $page = ($page == 0 ? 1 : $page);  
        $start = ($page - 1) * $per_page;                               
         
        $prev = $page - 1;                          
        $next = $page + 1;
        $lastpage = ceil($total/$per_page);
        $lpm1 = $lastpage - 1;
         
        $pagination = "";
        if($lastpage > 1)
        {   
            $pagination .= "<ul class='pagination'>";
                    $pagination .= "<li class='details'>Page $page of $lastpage</li>";
            if ($lastpage < 7 + ($adjacents * 2))
            {   
                for ($counter = 1; $counter <= $lastpage; $counter++)
                {
                    if ($counter == $page)
                        $pagination.= "<li><a class='active'>$counter</a></li>";
                    else
                        $pagination.= "<li><a href='".($url.$url_prefix.''.$counter)."'>$counter</a></li>";                    
                }
            }
            elseif($lastpage > 5 + ($adjacents * 2))
            {
                if($page < 1 + ($adjacents * 2))     
                {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='active'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='".($url.$url_prefix.''.$counter)."'>$counter</a></li>";                    
                    }
                    $pagination.= "<li class='dot'>...</li>";
                    $pagination.= "<li><a href='".($url.$url_prefix.''.$lpm1)."'>$lpm1</a></li>";
                    $pagination.= "<li><a href='".($url.$url_prefix.''.$lastpage)."'>$lastpage</a></li>";      
                }
                elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
                {
                    $pagination.= "<li><a href='".($url.$url_prefix.'1')."'>1</a></li>";
                    $pagination.= "<li><a href='".($url.$url_prefix.'2')."'>2</a></li>";
                    $pagination.= "<li class='dot'>...</li>";
                    for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='active'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='".($url.$url_prefix.''.$counter)."'>$counter</a></li>";                    
                    }
                    $pagination.= "<li class='dot'>..</li>";
                    $pagination.= "<li><a href='".($url.$url_prefix.''.$lpm1)."'>$lpm1</a></li>";
                    $pagination.= "<li><a href='".($url.$url_prefix.''.$lastpage)."'>$lastpage</a></li>";      
                }
                else
                {
                    $pagination.= "<li><a href='".($url.$url_prefix.'=1')."'>1</a></li>";
                    $pagination.= "<li><a href='".($url.$url_prefix.'=2')."'>2</a></li>";
                    $pagination.= "<li class='dot'>..</li>";
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                    {
                        if ($counter == $page)
                            $pagination.= "<li><a class='active'>$counter</a></li>";
                        else
                            $pagination.= "<li><a href='". ($url.$url_prefix.''.$counter)."'>$counter</a></li>";                    
                    }
                }
            }
             
            if ($page < $counter - 1){ 
                $pagination.= "<li><a href='".($url.$url_prefix.''.$next)."'>Next</a></li>";
                $pagination.= "<li><a href='".($url.$url_prefix.''.$lastpage)."'>Last</a></li>";
            }else{
                $pagination.= "<li><a class='active'>Next</a></li>";
                $pagination.= "<li><a class='active'>Last</a></li>";
            }
            $pagination.= "</ul>\n";      
        }
     
     
        return $pagination;
    } 
}
