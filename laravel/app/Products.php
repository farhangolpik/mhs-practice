<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Products extends Model {

	//
    
    
    public static function getProducts($search=array(),$offset=0, $per_page=0,$categoryid=0)
    {
		$result = array();
        $subSql="p.deleted=0";
        if(isset($search['key']))
        {
            $subSql.=" and u.key='".$search['key']."'";
        }
        
        if(isset($per_page) && $per_page>0)
        {
			 $limit_str = " LIMIT $offset, $per_page";  
		}
		else
		{
			$limit_str='';
		}
        if(isset($search['type']))
        {
            $subSql.=" and p.type='".$search['type']."'";
        }
        if(isset($categoryid) && $categoryid>0)
        {
            $catstr="LEFT JOIN products_categories pc ON pc.product_id=p.id ";
            $catwhere = 'AND pc.category_id='.$categoryid.' ';
        }
        else
        {
			$catwhere=' ';
			$catstr=' ';
		}
        
        $sql="select SQL_CALC_FOUND_ROWS p.id as id,p.sku as sku,p.image as images,p.name as name,p.teaser as teaser,p.price,p.salePrice,p.sale,p.description,p.requirments as requirments,p.keywords as keywords,u.key"
                . " from products as `p` left join urls as u on u.type_id=p.id ".$catstr
                . "where "
                . $subSql.$catwhere.$limit_str;
        if($limit_str=='')
        {
			return DB::select($sql);
		}
		else
		{
			$sql1="SELECT FOUND_ROWS() AS total";
            $result['results'] = DB::select($sql);
			$total_rows= DB::select($sql1);
			
			
			$result['total']= $total_rows;
			return  $result;
		}
        
    } 

}
