<?php

namespace App\Http\Controllers;
require_once("2checkout/Twocheckout.php");

use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,
    Input,
    Redirect;
use Auth;
use App\Products;
use App\User;
use App\Orders;
use App\Address;
use Config;
use App\Countries;
use App\OrdersProducts;
use App\ProductsCategories;
use App\OrdersBundles;
use Session;
use Illuminate\Http\Request;
use Stripe;
use App\StripeCustomers;
use App\QuestOrders;
use App\Quest\Api;
use App\Content;
use App\Cart;
use App\Functions\Functions;
use Intervention\Image\Facades\Image as Image;

//use Cartalyst\Stripe\Laravel\Facades\Stripe;

class CheckoutController extends Controller {

    public $auth;
    private $sessionId;

    public function __construct() {

       // $this->middleware('auth');
        session_start();
        $this->sessionId = session_id();
        
        
        
        
        // Your sellerId(account number) and privateKey are required to make the Payment API Authorization call.
        // orignal user key
		//Twocheckout::privateKey('3A9613C0-0A7F-403B-A7FE-9EADB200C6A5');
		// m.hassan@golpik.com test  user key
		\Twocheckout::privateKey('7EE2D572-4080-4BDF-B3EE-D5AA963619C5');
		//\Twocheckout::privateKey('3A9613C0-0A7F-403B-A7FE-9EADB200C6A5');
		//Twocheckout::sellerId('901248204');
		\Twocheckout::sellerId('901358688');
		//\Twocheckout::sellerId('103417321');
		// Your username and password are required to make any Admin API call.
		//\Twocheckout::username('medicalhealthsystems');
		\Twocheckout::username('hassankaloro007');
		\Twocheckout::password('Test@123');
		//\Twocheckout::password('7coQgG@wCR&5t5%U01t0');

		// If you want to turn off SSL verification (Please don't do this in your production environment)
		\Twocheckout::verifySSL(false);  // this is set to true by default

		// To use your sandbox account set sandbox to true
		\Twocheckout::sandbox(true);

		// All methods return an Array by default or you can set the format to 'json' to get a JSON response.
		\Twocheckout::format('json');
		
		
		
		
		
        //$this->paypal = new PayPal();
        // $this->_apiContext = $this->paypal->ApiContext(
        //        config('paypal.express.client_id'), config('paypal.express.secret'));
        // $this->_apiContext->setConfig(config('paypal.express.config'));
    }

    public function index() {
        //$shippings = Shipping::all();

        $coupon = array();
        $userId = Auth::user()->id;
        $user = User::findOrFail($userId);
        $address = Address::where('user_id', '=', $userId)->first();
        $countries = Countries::lists('name', 'id');


        if (empty($address)) {
            $address = new Address();
            $address->address = null;
            $address->address2 = null;
            $address->state = null;
            $address->city = null;
            $address->zip = null;
            $address->phone = null;
        }

        $cart = Session::get('cart');
        $countCart = Cart::countCart($this->sessionId);
        if ($countCart == 0) {
            return redirect("/");
        }

        return view('front.checkout.index', compact('countries', 'user', 'cart', 'address'));
    }

    public function order(Request $request) {
		
        //error_reporting(1);
        $userId = isset(Auth::user()->id)?Auth::user()->id:0;
        $service_ids = isset($request->service_ids)?$request->service_ids:array();
        if(isset($service_ids) && !empty($service_ids))
        {
				$validationArray = array(
				'firstName' => 'required|max:30',
				'lastName' => 'required|max:30',
				'email' => 'email|required|max:30',
				'country_id' => 'required|max:10',
				'state' => 'required|max:100',
				'city' => 'required|max:100',
				'address' => 'required|max:500',
				'address2' => 'max:500',
				
				'phone' => 'required|max:20',
				'user_picture' => 'required|mimes:jpeg,bmp,png,gif|max:2000',
				
			   
			   
			   
			  
				'message' => 'min:10|max:300',
			);
		}
		else
		{
				$validationArray = array(
				'firstName' => 'required|max:30',
				'lastName' => 'required|max:30',
				'email' => 'email|required|max:30',
				'country_id' => 'required|max:10',
				'state' => 'required|max:100',
				'city' => 'required|max:100',
				'address' => 'required|max:500',
				'address2' => 'max:500',
				
				'phone' => 'required|max:20',
				
				
			   
			   
			   
			  
				'message' => 'min:10|max:300',
			);
		}
        

        // d($request->all(),1);
        //$token = $request->stripeToken;
		if(isset($userId) && $userId>0)
		{
			$user = User::findOrFail($userId);
			$email = $user->email;
		}
		else
		{
			$user=array();
			$email='';
		}
        
        $validator = Validator::make($request->all(), $validationArray);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator->errors(), 'checkout')->withInput();
        }
        
       
        $account_create_check = (isset($request->account_create_check) && $request->account_create_check=='on')?1:0;
		
	

		
		
		
		
		
       
        
        DB::beginTransaction();
		 $fileName = array();
        try {

			if (Input::hasFile('user_picture')) {
				$file = Input::file('user_picture');

				// Making counting of uploaded images
				$file_count = count($file);
				if($file_count>1)
				{
				return redirect()->back()->withErrors('Only 1 files you can upload against single product')->withInput();
				}
				
					
				$destinationPath = public_path() . '/../../uploads/user_images/';
				$destinationPathThumb = $destinationPath . 'thumbnail/';
				$fileName = Functions::saveImage($file, $destinationPath, $destinationPathThumb);
				$upload = Image::make($destinationPath . $fileName)->fit(280)->save($destinationPathThumb . $fileName);
				

			}
			if(isset($fileName) && !empty($fileName))
			{
				$fileName = $fileName;
			}
			else
			{
				$fileName='';
			}
			
            $orderModel = new Orders();
            $orderModel->firstName = $request->firstName;
            $orderModel->lastName = $request->lastName;
            //$orderModel->dob = $request->year . "-" . $request->month . "-" . $request->date;
            //$orderModel->gender = $request->gender;
            $orderModel->email = $request->email;
            $orderModel->order_image = $fileName;
            $orderModel->message = $request->message;
            $orderModel->user_id = isset(Auth::user()->id)?Auth::user()->id:0;
            $orderModel->grandTotal = $request->grandTotal;
            //$orderModel->paymentType = 'stripe';
            $orderModel->save();
            $order_id = $orderModel->id;
            
			try {
            $charge = \Twocheckout_Charge::auth(array(
                "sellerId" => "901358688",
                "merchantOrderId" => $order_id,
                "token" => $_POST['token'],
                "mode" => '2CO',
                "currency" => 'USD',
                "total" => $request->grandTotal,
                "metadata" => [
                    "Order ID" =>  $order_id,
                    "Link" => url('admin/order/' . $order_id)
                ],
                "billingAddr" => array(
                    "name" => $request->firstName.' '.$request->lastName,
                    "addrLine1" => $request->address,
                    "city" => $request->city,
                    "state" => $request->state,
                    "zipCode" => '43123',
                    "country" => 'USA',
                    
                    "email" => $request->email,
                    "phoneNumber" => $request->phone
                ),
                "shippingAddr" => array(
                    "name" => $request->firstName.' '.$request->lastName,
                    "addrLine1" => $request->address,
                    "city" => $request->city,
                    "state" => $request->state,
                    "zipCode" => '43123',
                    "country" => 'USA',
                    "email" => 'testingtester@2co.com',
                    "phoneNumber" => '555-555-5555'
                )
            ));
           
        } catch (Twocheckout_Error $e) {
            return redirect()->back()->withErrors($e->getMessage(), 'checkout')->withInput();
        }
        //$charge = json_decode($charge);
		//d($charge,1);
            $user_id_new = (isset(Auth::user()->id) && Auth::user()->id>0)?Auth::user()->id:0;
            if(isset($account_create_check) && $account_create_check ==1)
            {
				$UserModel = new User();
				$UserModel->email=$request->email;
				$UserModel->password='Test@123';
				$UserModel->user_image=$fileName;
				$UserModel->save();
                $user_id_new = $UserModel->id;
			}
            if((isset($user_id_new) && $user_id_new>0) )
            {
				$address1 = new Address();
				$address1->country = $request->country_id;
				$address1->city = $request->city;
				$address1->state = $request->state;
				$address1->address = $request->address;
				$address1->address2 = isset($request->address2)?$request->address2:'';
				$address1->order_id = $order_id;
				$address1->user_id = $user_id_new;
				//$address1->zip = $request->zip;
				$address1->phone = $request->phone;
				//$address1->addressType = 'patient';
				$address1->save();
			}
            

            $quest['PATIENT_LASTNAME'] = $request->lastName;
            $quest['PATIENT_FIRSTNAME'] = $request->firstName;
            $quest['PID'] = 'P' . $order_id;
            //$quest['DOB'] = $request->year . sprintf("%02d", $request->month) . sprintf("%02d", $request->date);
            //$quest['GENDER'] = strtoupper($request->gender);
            $quest['PHONE'] = strtoupper($request->phone);
            //$quest['SSN'] = $request->ssn;
            //$quest['MESSAGE_CODE'] = 'ORM^O01';
            //$quest['MESSAGE_CONTROL_ID'] = $orderPrefix . $order_id;

            //$quest['ORDER_CONTROL'] = 'NW';
            $quest['ORDER_NUMBER'] = $order_id;
            $cart = Session::get('cart');

            /*$charge = Stripe::charges()->create([
                'amount' => $request->grandTotal,
                'currency' => 'usd',
                'customer' => $stripeCustomerId,
                'source' => $defaultSource,
                'metadata' => [
                    "Order ID" => $orderPrefix . $order_id,
                    "Link" => url('admin/order/' . $order_id)
                ],
                'capture' => true]);
			*/

            $sum = 0;
            $quantity = 0;
            $tests = array();
            $testCount = 1;
            foreach ($cart as $product) {

                $opModel = new OrdersProducts();
                $opModel->product_id = $product->product_id;
                $opModel->price = $product->total_price;
                $opModel->order_id = $order_id;
                $opModel->quantity = $product->quantity;
                $opModel->save();

                $productModel = Products::find($product->product_id);

                if ($productModel->type == 'service') {

                    $productCategories = ProductsCategories::getBundleCategories($productModel->id);
                    // d($productCategories,1);
                    foreach ($productCategories as $pc) {
                        $input['name'] = $productModel->name . '-' . $pc->category . '-' . $pc->name;
                        $input['product_id'] = $productModel->id;
                        $input['order_id'] = $order_id;
                        $input['created_at'] = date('Y-m-d H:i:s');
                        $id = OrdersBundles::insertGetId($input);
                        $tests[$testCount] = 'OBR|' . $testCount . '|' . $order_id . '||^^^B' . $productModel->id . '^' . $input['name'] . '|||{{TIME}}|||||||||||||||||||||';
                        $testCount++;
                    }
                } elseif ($productModel->type == 'simple') {

                    $tests[$testCount] = 'OBR|' . $testCount . '|' . $order_id . '||^^^' . $productModel->sku . '^' . $productModel->name . '|||{{TIME}}|||||||||||||||||||||';
                    $testCount++;
                }
            }

            $quest['TESTS'] = implode("\r", $tests);

            if ($order_id > 350) {
                return redirect('checkout/fail');
            }

            //$questOrder = Api::submitOrder($quest, $order_id);
            //QuestOrders::insertGetId(array('order_id' => $order_id, 'response' => $questOrder));
            Session::put('order_id', $order_id);
            DB::commit();
            return redirect('checkout/success/' . $order_id);
        } catch (Exception $e) {
            DB::rollBack();
            return redirect('checkout/fail');
        }
    }

    public function success($id) {

        $order = Orders::getOrderDetailByPk($id);
        $orderPrefix = Config::get('params.order_prefix');
        //d($order,1);
        $content = Content::where('code', '=', 'order_confirmation')->get();
        $replaces = array();
        $template = Functions::setEmailTemplate($content, $replaces);
        $message = 'You have received an order from ' . $order->patientName . '. Their order is as follows:';
        $link = url('') . '/admin/order/' . $id;
        $body = view('front.orders.email', compact('order', 'message', 'link'))->with('id', $id);
        //$mail = Functions::sendEmail(Config::get('params.from_email'),$template['subject'], $body);
        $message = '';
        $link = url('') . '/order/' . $id;
        $body = view('front.orders.email', compact('order', 'message', 'link'))->with('id', $id);
        //$mail = Functions::sendEmail($order->email, $template['subject'], $body);
        Cart::where('session_id', '=', $this->sessionId)->delete();
        return view('front.checkout.success')->with('id', $id);
    }

    public function fail() {
        return view('front.checkout.fail');
    }

}
