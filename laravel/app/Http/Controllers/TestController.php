<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Validator,
    Input,
    Redirect;
use Auth;
use App\User;
use Illuminate\Http\Request;
use App\Orders;
use App\Quest\Api;
use App\OrdersDocuments;
use DB;
use Config;

class TestController extends Controller {

    public function __construct() {
        //$this->middleware('auth');
    }

    public function index() {

        $username = "HORI37220test";
        $password = "06hori37220";
        $documentEndpoint = "https://cert.hub.Care360.com/rest/orders/v1/document";

        //$orders = Orders::get();

        $orders = DB::select("select id from orders where id not in (select order_id from orders_documents)");

        $i = 0;
        foreach ($orders as $order) {

            $file = '/var/www/html/ncl/uploads/quest/orders/' . $order->id . '.hl7';

            if (file_exists($file) == 1) {

                $message = file_get_contents($file, 'r');

                $array = explode("\r", $message);

                $orderPrefix = Config::get('params.order_prefix');
                $array[0] = "MSH|^~\&||97513297|PSC|MET|" . date('YmdHi') . "||ORM^O01|" . $orderPrefix . $order->id . "|P|2.3";
                $array[] = "IN1|1||||^^^^||||||||||| ^^|||^^^^||||||||||||||||||||||||||||C";
                $message = implode("\r", $array);
                $message = base64_encode($message);
                $request = '{
                            "orderHl7": "' . $message . '"
                        }';

                $authorization = base64_encode($username . ":" . $password);
                $headers = array('Content-type: application/json', 'Authorization: Basic ' . $authorization);
                $res = self::makeCurlRequest($documentEndpoint, $headers, 1, $request);
                $responseArray = json_decode($res);
                $documentData = $responseArray->orderSupportDocuments[0]->documentData;
                //file_put_contents($path, $documentData);
                $decoded = base64_decode($documentData);
                $decoded = base64_decode($decoded);
                $fp = fopen('/var/www/html/ncl/uploads/quest/orders/documents/' . $order->id . '.pdf', 'w+');
                fwrite($fp, $decoded);
                fclose($fp);


                $headers = array(
                    'Content-type: text/plain',
                    'Authorization: Basic ' . $authorization,
                );
                $response = self::makeCurlRequest("https://cert.hub.care360.com/rest/orders/v1/submission", $headers, 1, $message);

                $fp = fopen('/var/www/html/ncl/uploads/quest/orders/psc/' . $order->id . '.hl7', 'w+');
                fwrite($fp, $message);
                fclose($fp);


                $model = new OrdersDocuments();
                $model->order_id = $order->id;
                $model->created_at = date('Y-m-d H:i:s');
                $model->response = base64_decode($response);
                $model->save();
            }
            $i++;
        }
    }

    public static function makeCurlRequest($documentEndpoint, $headers, $post = 0, $encodeMessage) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $documentEndpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        if ($post == 1) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $encodeMessage);
        }
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

}
