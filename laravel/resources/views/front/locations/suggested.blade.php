<?php
if (count($locations) > 0) {
    ?>
    <div class="container">  
        <ul>
                <?php
                foreach ($locations as $location) {
                    ?>
                    <li>
                        <a href="{{url('location')}}/<?php echo $location->id ?>"><?php echo $location->address ?>, <?php echo $location->city ?>, <?php echo $location->state ?>, <?php echo $location->zip ?><li>
                    <?php
                }
                ?>
         </ul>
    </div>
    <?php
}
?>