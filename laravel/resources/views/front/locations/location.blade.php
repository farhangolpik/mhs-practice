@extends('front')

@section('content')


<section class="inr-bnr-blank">
    <div class="container">
        <div class="hed">
			<h2><?php echo $location->name ?> <?php echo $location->city ?> <?php echo $location->address2 ?></h2>
            <span><?php echo $location->address ?>, <?php echo $location->city ?>, <?php echo $location->zip ?></span>
		</div>
    </div>
</section>
 


<section class="location-area pt30 pb30">
    <div class="container">

        <div class="map-area col-sm-8">

            <div id="map" style="height: 450px;"></div> 
            <!--
            <iframe src="https://www.google.com/maps/d/embed?mid=1JqtD_I0ZCwHSovJXCFHLwFBnQGI&hl=es&z=20&ll=" width="640" height="480"></iframe>

           
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d387145.86626760714!2d-74.25818793633874!3d40.70531105497054!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY!5e0!3m2!1sen!2s!4v1477480451017" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            -->
        </div>
        <div class="lab-location-area col-sm-4">
<!--
            <div class="lab-location-area col-sm-12">
                <div class="text-center clrhm"><h2>LAB LOCATION</h2></div>
               include('front/locations/search')
            </div>
-->
		<div class="loc__contact-info list-icon clrlist listview">
            <ul>
                <li> <i class="fa fa-clock-o"></i> <strong>Operation Hours :</strong><br> <?php echo $location->operationHours ?></li>
                <li> <i class="fa fa-phone"></i> <strong>Phone :</strong> <?php echo $location->phone ?> </li>
                <li> <i class="fa fa-fax"></i>  <strong>Fax :</strong> <?php echo $location->fax ?> </li>
                <!--<li><strong>Directions :</strong> <?php echo $location->description ?> </li>-->
            </ul>
		</div>
		
        </div>
    </div>
</section>
<section class="box-area text-center pt20 pb50">
    <div class="container">

        <div class="box col-sm-4 anime-left ">
            <div class="box__img">
                <img src="{{ asset('front/images/pic.jpg') }}" alt="" />
            </div>
            <div class="box__cont ">
                <p>We make getting blood draws Easy and convenient for you</p>
            </div>				
        </div>


        <div class="box col-sm-4 anime-in">
            <div class="box__img">
                <img src="{{ asset('front/images/postBanner.png') }}" alt="" />
            </div>
            <div class="box__cont">
                <p>No appointments needed Walk-ins are welcome You are in and out!</p>
            </div>				
        </div>


        <div class="box col-sm-4 anime-right">
            <div class="box__img">
                <img src="{{ asset('front/images/latestPost2.png') }}" alt="" />
            </div>
            <div class="box__cont">
                <p>Access your results in 72 hour</p>
            </div>				
        </div>


    </div>
</section>
<script>
    function initMap() {
        var uluru = {lat: <?php echo $location->latitude ?>, lng: <?php echo $location->longitude ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 17,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map,
            title: '<?php echo $location->address ?>'
        });
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA1qKtXM-NvTrUxGfYpdkR3tCkGJMkIaq4&callback=initMap">
</script>
@endsection