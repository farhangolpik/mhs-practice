@extends('front')

@section('content')

@include('front/common/page_banner')
<section class="login-section col-sm-12 pt60">
		  
		  <div class="login-form fom col-sm-8 col-sm-offset-2 placeholder--hover pr50 pl50 overload">
			
			<div class="hed lv2 mb30">
				<h2>Login</h2>
			</div>
			@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
			  <form id="loginform" class="login__form"  method="POST" action="{{ url('postLogin') }}" >
			   <input type="hidden"  name="_token" value="{{ csrf_token() }}">
				<div class="form-group / in-view">
					 <input type="text" class="form-control" name="email" placeholder="Username*" required="">
		        </div>
				<div class="form-group / in-view">
		            <input type="password" class="form-control" name="password" placeholder="Password*" required="">
		        </div>
				<div class="form-group / in-view">
					 <input type="checkbox" class="form-control has-value">Remember me
				 </div>
				<div class="login-btn / in-view">
					<button type="submit" onclick="document.getElementById('loginform').submit();" class="btn btn-primary w100">Login</button>
		        </div>
				
			  </form>
			  <div class="col-sm-12 fpwd mt20 mb40 text-center / in-view">
					<a href="<?php echo url('register'); ?>">Register?</a>
			  </div> 
			  <div class="clearfix / in-view"></div>
			
		   </div>
		   
	</section>
@endsection
