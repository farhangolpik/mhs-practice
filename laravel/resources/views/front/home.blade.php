@extends('signup')
<?php
$title = 'Home';
$description = '';
$keywords = '';
?>
@include('front/common/meta')

@section('content')

	
<section id="form"><!--form-->
		<div class="container">	<div class="row">  	
	    		<div class="col-sm-12">
	    			<div class="contact-form">
	    				<h2 class="title text-center">Search Cafes</h2>
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form id="main-contact-form" class="contact-form row" name="contact-form" method="post">
				            
				            <div class="form-group col-md-12">
				                <textarea name="message" id="message" required="required" class="form-control" rows="2" placeholder="Search Here"></textarea>
				            </div>                        
				            <div class="form-group col-md-12">
				                <h2 class="title text-center"><input type="submit" name="submit" class="btn btn-primary" value="Submit"></h2>
				            </div>
				        </form>
	    			</div>
	    		</div>
	    		   			
	    	</div> 
		</div>    			
	  </section>
	  
@endsection