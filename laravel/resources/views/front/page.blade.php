
@extends('front')

@section('content')
@include('front/common/sliderhomepage')
	
	
	<section class="srv-area bg-cvr dark-area" style="background-image:url('front/images/srv-bg.png')">
		<div class="container">
			
			<div class="hed underline--plus">
				<h2>SERVICES</h2>
			</div>
			
			
			<div class="srvtabs-area tabs-change--hover">
				<ul class="nav nav-tabs">
				  <li>
					  <a href="#srvtab1" data-toggle="tab">
						<i class="icon"><img src="{{ asset('front/images/srvicon1.png')}}" alt="" /></i>
						<span>DRUGS & COMMODITIES</span>				  
					  </a>
				  </li>
				  <li>
					  <a href="#srvtab2" data-toggle="tab">
						<i class="icon"><img src="{{ asset('front/images/srvicon2.png')}}" alt="" /></i>
						<span>REVERSE LOGISTICS</span>				  
					  </a>
				  </li>
				  <li>
					  <a href="#srvtab3" data-toggle="tab">
						<i class="icon"><img src="{{ asset('front/images/srvicon3.png')}}" alt="" /></i>
						<span>HEALTH ADVISORY</span>				  
					  </a>
				  </li>
				 <!-- <li>
					  <a href="#srvtab4" data-toggle="tab">
						<i class="icon"><img src="images/srvicon4.png" alt="" /></i>
						<span>ENDOCRINOLOGY</span>				  
					  </a>
				  </li> -->
				</ul> 
				
				<div class="tab-content overload">
				  <div class="tab-pane active" id="srvtab1">
					
					<div class="srvtab__cont col-sm-7 cont list-icon--arr clrlist listview">
						<h4>DRUGS & COMMODITIES</h4>
						<p>Lorem Ipsum drugs is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley .</p>
						
						<ul>
							<li>Lorem Ipsum is simply dummy text of the printing.</li>
							<li>Typesetting industry. Lorem Ipsum has been the industry's standard.</li>
							<li>Dummy text ever since the 1500s, when an unknown printer took a galley.</li>
						</ul>
					</div>
					<div class="srvtab__img col-sm-5 img">
						<div class="__img">
							<img src="{{ asset('front/images/tabpic1.jpg')}}" alt="" />
						</div>
					</div>
						
				  
				  </div>
				  <div class="tab-pane" id="srvtab2">
				  
				  
					<div class="srvtab__cont col-sm-7 cont list-icon--arr clrlist listview">
						<h4>REVERSE LOGISTICS</h4>
						<p>Lorem Ipsum reverse is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley .</p>
						
						<ul>
							<li>Lorem Ipsum is simply dummy text of the printing.</li>
							<li>Typesetting industry. Lorem Ipsum has been the industry's standard.</li>
							<li>Dummy text ever since the 1500s, when an unknown printer took a galley.</li>
						</ul>
					</div>
					<div class="srvtab__img col-sm-5 img">
						<div class="__img">
							<img src="{{ asset('front/images/tabpic1.jpg')}}" alt="" />
						</div>
					</div>
					
				  </div>
				  <div class="tab-pane" id="srvtab3">
				  
					<div class="srvtab__cont col-sm-7 cont list-icon--arr clrlist listview">
						<h4>HEALTH ADVISORY</h4>
						<p>Lorem Ipsum health is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley .</p>
						
						<ul>
							<li>Lorem Ipsum is simply dummy text of the printing.</li>
							<li>Typesetting industry. Lorem Ipsum has been the industry's standard.</li>
							<li>Dummy text ever since the 1500s, when an unknown printer took a galley.</li>
						</ul>
					</div>
					<div class="srvtab__img col-sm-5 img">
						<div class="__img">
							<img src="{{ asset('front/images/tabpic1.jpg')}}" alt="" />
						</div>
					</div>
					
					</div>
					
			<!--	  <div class="tab-pane" id="srvtab4">
				  
					<div class="srvtab__cont col-sm-7 cont list-icon--arr clrlist listview">
						<h4>ENDOCRINOLOGY</h4>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley .</p>
						
						<ul>
							<li>Lorem Ipsum is simply dummy text of the printing.</li>
							<li>Typesetting industry. Lorem Ipsum has been the industry's standard.</li>
							<li>Dummy text ever since the 1500s, when an unknown printer took a galley.</li>
						</ul>
					</div>
					<div class="srvtab__img col-sm-5 img">
						<div class="__img">
							<img src="images/tabpic1.jpg" alt="" />
						</div>
					</div>
					
				  </div> -->
				  
				</div>
			</div>
			
		</div>
	</section>
	
	
	
	
	<section class="prod-area bg-cvr" style="background-image:url('front/images/prod-bg.jpg')">
		<div class="container">
			
			<div class="hed underline--plus">
				<h2>MEDICAL PRODUCTS</h2>
			</div>
			
			<!--<div class="prod__seachbar form-group">
				<div class="input-group">
					<select class="form-control">
						<option>- Select  Product Type -</option>
						<option>- Select  Product Type -</option>
						<option>- Select  Product Type -</option>
						<option>- Select  Product Type -</option>
						<option>- Select  Product Type -</option>
					</select>
					<span class="input-group-addon w30">
						<button>FIND PRODUCT</button>
					</span>
				</div>
				
			</div>
			-->
			
			<div class="prod-main col-sm-11-5 pul-cntr">
			<h3>CONSUMABLES</h3>
			<div class="prod-left prod-sideimg col-sm-4 p0">
				<img src="{{ asset('front/images/prod-left.jpg')}}" alt="" />			
			</div>
			<?php if(isset($products) && !empty($products)) { ?>
				<div class="prod-right prod-group col-sm-8 bg-white p0">
					
					<?php  foreach($products as $key=>$val ) { 
						
						$product_images=array();
						
						if(isset($val->image) && $val->image!='')
						{
							$product_images = explode(',',$val->image);
						}
						?>
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							<div class="prodbox__cont">
								<h4><?php echo $val->name; ?></h4>
								<div class="cont"><?php echo $val->teaser; ?></div>
							</div>
							<div class="prodbox__img">
								<?php if(isset($product_images) && !empty($product_images)) { ?>
								<img src="{{ asset('uploads/products')}}/<?php echo (isset($product_images[0]) && $product_images[0]!='')?$product_images[0]:'';  ?>" alt="<?php echo (isset($product_images[0]) && $product_images[0]!='')?$product_images[0]:'';  ?>" />
								<?php } ?>
							</div>
							<div class="prodbox__actions clrlist listview">
								<ul>
									<!--<li><a href="#"><i class="fa fa-heart"></i></a></li>-->
									<li><a onclick="add_to_cart_button(<?php echo isset($val->id)?$val->id:0?>)" href="javascript:void(0);"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="<?php echo url('product/'.$val->id)?>"><i class="fa fa-search"></i></a></li>
								</ul>
							</div>
						</div>
					</div>
					<?php } ?>
					
					
					
					
					
					
					
					
					
					
					
					
					
					
					
				
				</div>
			
			<?php } ?>
			
			
			
			
			
			
			
			
			
			
			
			</div>
			
		</div>
	</section>
	
	<section class="homecontact-area bg-cvr dark-area" style="background-image:url('front/images/contact-bg.png')">
		<div class="container">
			
			<div class="contact-form fom col-sm-12 placeholder--hover gray-inputs">
		   @if($errors->has())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if (Session::has('success'))
        <div class="alert alert-success">
            <h4><i class="icon fa fa-check"></i> &nbsp  {!! session('success') !!}</h4>
        </div>
        @endif
        
			<div class="hed underline--plus">
				<h2>GET IN TOUCH WITH US</h2>
			</div>
			
			<form role="form" method="POST" action="{{ url('contact-send') }}" class="contact__form">
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="fom fom-home row">
			
				<div class="form-group col-sm-6">
					<input value="{{ old('firstname')?old('firstname'):'' }}"  name="firstname" class="form-control" placeholder="First Name" />
				</div>
				
				<div class="form-group col-sm-6">
					<input value="{{ old('lastname')?old('lastname'):'' }}" name="lastname" class="form-control" placeholder="Last Name" />
				</div>
				
				<div class="form-group col-sm-6">
					<input value="{{ old('email')?old('email'):'' }}" name="email" class="form-control" placeholder="Email" />
				</div>
				
				<div class="form-group col-sm-6">
					<input value="{{ old('phone')?old('phone'):'' }}" name="phone" class="form-control" placeholder="Phone Number" />
				</div>
				
				<div class="form-group col-sm-12">
					<textarea id="message2" name="message" class="form-control mh" placeholder="Message Here!" rows="4" ><?php echo old('message')?old('message'):''?></textarea>
				</div>
				
				<div class="btns col-sm-12 text-center">
					<button onclick="$(this).closest('form').submit();" class="btn btn-default btn-wide">SEND</button>
				</div>
			
				</form>
			</div>
			
		</div>
	</section>

<script>
function add_to_cart_button(id)
{
	
	if(id>0)
	{
		var quantity_numner = 1;
	  $.post("<?php echo url('add-to-cart-icon'); ?>",
				{
					quantity_numner: quantity_numner,
					id: id,
				   
					_token: '{{ csrf_token() }}'
				},
				function(data, status){
					var obj = jQuery.parseJSON( data );
				  
					//console.log(obj.error);return false;
					if(obj && obj>0)
					{
						$('#counter_cart').html(obj);
						alert('Product added successfully');
						return false;
					}
					else if(obj==-1)
					{
						alert('Product is not available');
						return false;
						
					}
					else if(obj==0)
					{
						
						return false;
					}
					else
					{
						return false;
					}
				});
	}
	else
	{
		return false;
	}
	
}
</script>

@endsection
