@extends('front')

@section('content')

@include('front/common/page_banner')
<?php
use App\Functions\Functions;
?>
<section class="products-page last-section bg-white">

	
	@include('front/common/left_categories')	
		<!--categories end-->
		
				
	<div class="prod__right col-sm-9 girdbox-area ">		
			
			
			<div class="sorting-bar col-sm-12 p0 global-active mb20">
			
				<div class="gridbox-icons icon2x col-sm-3" id="girdBox">
					<button id="girdBoxThumbStyle"><i class="glyphicon glyphicon-th-large"></i></button>
					<button id="girdBoxListStyle"><i class="glyphicon glyphicon-th-list"></i></button>
				</div>
				
				<!--<div class="sort col-sm-5">
					<div class="input-group">
					<span class="input-group-addon">Sort By</span>
					<select class="form-control">
						<option>Position</option>
						<option>Position</option>
						<option>Position</option>
					</select>
					</div>
				</div>
				-->
				
				<div class="pagi-area col-sm-4 text-right">
					<?php 
						$tempPageurl =   $_SERVER['REQUEST_URI'];
						if (strpos($tempPageurl, 'screen-') !== false) {
						$screen_array = explode("/screen-", $tempPageurl);
						$page_url = $screen_array[0];
						}   
						else
						{
						$page_url = $tempPageurl;
						} 

						$querystring=$page_url; 

						echo Functions::pagination($result_count, $per_page,$page_offset, $querystring,'/screen-');
					?>
					<!--<ul class="pagination">
					  <li class="active"><a href="#">1</a></li>
					  <li><a href="#">2</a></li>
					  <li><a href="#">3</a></li>
					  <li><a href="#">&raquo;</a></li>
					</ul>-->
				</div>
				
			</div>

		
		<div class="row">
		
			<div class="prod-group">
			
			
			<?php  if(isset($products) && !empty($products)) { 
				foreach($products as $key=>$product_alpha) { 
				if(isset($product_alpha) && !empty($product_alpha))
				{
					foreach($product_alpha as $k=>$val)
					{
						$product_images=array();
						
						if(isset($val->images) && $val->images!='')
						{
							$product_images = explode(',',$val->images);
						}
				
				?>
					<div class="prodbox col-sm-4">
						<div class="prodbox__inr">
							
							<div style="cursor:pointer;" onclick="window.location='<?php echo url('product/'.$val->id); ?>'" class="prodbox__img">
								<?php if(isset($product_images) && !empty($product_images)) { ?>
								<img src="{{ asset('uploads/products')}}/<?php echo (isset($product_images[0]) && $product_images[0]!='')?$product_images[0]:'';  ?>" alt="<?php echo (isset($product_images[0]) && $product_images[0]!='')?$product_images[0]:'';  ?>" />
								<?php } ?>
							</div>
							
							<div class="prodbox__cont">
								<h4 style="cursor:pointer;" onclick="window.location='<?php echo url('product/'.$val->id); ?>'" ><?php echo $string_name = (strlen($val->name) > 20) ? substr($val->name,0,17).'...' : ucfirst($val->name); ?></h4>
								<div class="cont"><?php echo $string = (strlen($val->teaser) > 60) ? substr($val->teaser,0,50).'...' : ucfirst($val->teaser); ?></div>
								<div class="prod__price">
									<span><?php echo (config('params.currency.USD.symbol'))?config('params.currency.USD.symbol'):'₦'; ?></span> <?php echo $val->price; ?>
								</div>
								
								<div class="prod__atc">
									<a onclick="add_to_cart(<?php echo $val->id; ?>)" href="javascript:void(0);" class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</a>							
									<form id="form_add_to_cart<?php echo $val->id; ?>" class="prod-detail-form">
									<input type="hidden" value="1" name="quantity" id="quantity"  />
									<input type="hidden" name="total_price"  id="total_price" value="<?php echo isset($val->price)?ucfirst($val->price):'0.00'; ?>" />
									<input type="hidden" name="price<?php echo $val->id; ?>"  id="price<?php echo $val->id; ?>" value="<?php echo isset($val->price)?ucfirst($val->price):'0.00'; ?>" />
									<input type="hidden" name="product_id"  id="product_id" value="<?php echo isset($val->id)?ucfirst($val->id):0; ?>" />
									</form>
								</div>
							</div>
							
							<div class="prodbox__actions clrlist listview">
								<ul>
									
									<!--<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
									<li><a href="#"><i class="fa fa-search"></i></a></li>-->
								</ul>
							</div>
							
						</div>
					</div>
					
				<?php } } } }else{ echo 'Products not found'; } ?>	
					
					
					
					
					
			
			
			</div>
				
		</div>
	</div>
				
				@include('front/common/related_products')
<script>
function add_to_cart(id)
{
	var price = $('#total_price'+id).val();
            var form = $('#form_add_to_cart'+id).serialize();

            //    return false;
            var jqxhr = $.get('<?php echo url('cart/add'); ?>', form, function () {
                // alert( "Product added to cart." );
                
                window.location = '<?php echo url('cart/view'); ?>';

            })
                    .done(function () {
                        //alert( "second success" );
                    })
                    .fail(function () {
                        //alert( "error" );
                    })
                    .always(function () {
                        //alert( "finished" );
                    });
}
    

</script>				
</section>

@endsection
