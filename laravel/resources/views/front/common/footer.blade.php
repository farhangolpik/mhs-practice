<footer>
		<section class="ftr-area ftr--blind" id="footer">
			<div class="container">
			
				<div class="ftr__box col-sm-3 ftr__logo">
					<h4><img src="{{ asset('front/images/ftr-logo.png')}}"></h4>
					<div class="cont">
					Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever make a type specimen book.Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
					</div>					
				</div>
				<div class="ftr__box ftr__nav col-sm-3 dotlist listview clrlist">
					<h4>SITEMAP</h4>
					<ul>
						<li><a href="{{ url('home') }}">Home</a></li>
						<li><a href="{{ url('products') }}">Products</a></li>
						<li><a href="{{ url('laboratory-services') }}">Laboratory Services</a></li>
						<li><a href="{{ url('services') }}">Services</a></li>
						<li><a href="{{ url('contact') }}">Contact Us</a></li>
					</ul>
				</div>
				
				<div class="ftr__box ftr__nav col-sm-3 dotlist listview clrlist">
					<h4>SUPPORT</h4>
					<ul>
						<li><a href="">Privacy Policy</a></li>
						<li><a href="">System requirements</a></li>
						<li><a href="">Terms and Conditions</a></li>
						<li><a href="">Region / Country</a></li>
						<li><a href="">Registration Information</a></li>
					</ul>
				</div>
				
				<div class="ftr__box ftr__nav col-sm-3 clrlist listview list-icon">
					<h4>CONTACT US</h4>
					<ul>
						<li><i class="fa icon"><img src="{{ asset('front/images/ftricon1.png')}}" alt="" /></i> <a href="#"><span>Abuja Office, 12 Sheik Ismail<br>Idris  Street, Near NNPC Mega<br>&nbsp; Station, First Avenue, Gwarinmpa,<br>&nbsp; Abuja, FCT</span></a></li>
						<li><i class="fa icon"><img src="{{ asset('front/images/ftricon2.png')}}" alt="" /></i> <a href="#"><span>+234 (0) 90 99 60 1329</span></a></li>
						<li><i class="fa icon"><img src="{{ asset('front/images/ftricon3.png')}}" alt="" /></i> <a href="#"><span>info@medicarehealthsystems.com </span></a></li>
					</ul>
				</div>
		
			</div>
		</section>
		
		
		<section class="bottom-area">
			<div class="container">
				
				<div class="fl"> &copy; Medicare Health Systems. All rights reserved.</div>
				
				<!-- <div class="fr">Design & Developed by <a href="golpik.com">Golpik</a></div> -->
				
			</div>
		</section>
		
		<a href="" class="scrollToTop"><i class="fa fa-angle-up"></i></a>
		
	</footer>
