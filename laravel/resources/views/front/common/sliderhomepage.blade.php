<section class="slider-area hover-ctrl fadeft " >
		<div id="carousel-example-generic" class="carousel slide slider--pauseplay" data-ride="carousel" data---interval="false">
		
		  <ol class="carousel-indicators thumbs">
			<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
			<li data-target="#carousel-example-generic" data-slide-to="1"></li>
			<li data-target="#carousel-example-generic" data-slide-to="2"></li>
		  </ol>

		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">


			<div class="item active bg-cntr" style="background-image:url({{ asset('front/images/slide1.jpg') }} );">
			  
			  <div class="caro-cap col-sm-5">
				  
				  	<h3>Trust Worthy & Timely</h3>
					<h1>HEALTH & <span>MEDICAL</span></h1>
					<div class="cont">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</div>
					<a class="lnk-btn btn btn-primary">READ MORE...</a>
				  
			  </div>
			  
			</div>
			
			
			<div class="item bg-cntr" style="background-image:url({{ asset('front/images/slide1.jpg') }} );">
			  
			  <div class="caro-cap col-sm-5">
				  
				  	<h3>Trust Worthy & Timely</h3>
					<h1>HEALTH & <span>MEDICAL</span></h1>
					<div class="cont">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</div>
					<a class="lnk-btn btn btn-primary">READ MORE...</a>
				  
			  </div>
			  
			</div>
			
			<div class="item bg-cntr" style="background-image:url({{ asset('front/images/slide1.jpg') }});">
			  
			  <div class="caro-cap col-sm-5">
				  
				  	<h3>Trust Worthy & Timely</h3>
					<h1>HEALTH & <span>MEDICAL</span></h1>
					<div class="cont">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</div>
					<a class="lnk-btn btn btn-primary btn-wide">READ MORE...</a>
				  
			  </div>
			  
			</div>
			
			
			
		  </div>

		  <a class="left carousel-control hidden" href="#carousel-example-generic" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left"></span>
		  </a>
		  <a class="right carousel-control hidden" href="#carousel-example-generic" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right"></span>
		  </a>
		  
		   <div id="carouselButtons" class="pul-rgt">
				  <button id="playButton" type="button" class="btn btn-default btn-xs is-active">
					  <span class="glyphicon glyphicon-play"></span>
				   </button>
				  <button id="pauseButton" type="button" class="btn btn-default btn-xs">
					  <span class="glyphicon glyphicon-pause"></span>
				  </button>
 			</div>
			
		  
		</div>
		
		
	</section>
	
	
	
