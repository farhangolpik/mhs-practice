<?php
use App\Categories;
use App\Functions\Functions; 
use App\Cart;



$allCategories = Categories::orderBy('parent_id','asc')->get();


$allCategoriesServices = Categories::getServiceCategories();

$categories = Functions::getCategories($allCategories);
$servicesTmp = Functions::getCategories($allCategoriesServices);

$countCart = Cart::countCart(session_id());

?>
	<header>
	
		<section class="hdr-top bdr-grid bg-white">
			<div class="container0">
				
				<div class="top__contactinfo col-sm-7 clrlist bg-white p0">
					<ul>
						<li><a href="#"><i class="fa fa-phone"></i> <span>+234 (0) 90 99 60 1329</span></a></li>
						<li><a href="#"><i class="fa fa-map-marker"></i> <span>Abuja Office, 12 Sheik Ismail Street</span></a></li>
						<li><a href="#"><i class="fa fa-clock-o"></i> <span>MON - SAT : 8 A.M - 9.00 P.M</span></a></li>
						
					</ul>
				</div>
				
				
				<div class="top__social col-sm-5 clrlist text-right bg-white p0">
				
					<ul>
						<li class="shopping"><a href="<?php echo url('cart/view'); ?>"><i class="fa fa-shopping-cart"></i><span class="badge"><?php echo (isset($countCart) && $countCart>0)?$countCart:0; ?></span></a></li>
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
					</ul>
				</div>
				
			</div>
		</section>
	
	
	
	<section class="hdr-area hdr-nav  cross-toggle navbar-overide">
		<div class="container">
			<nav class="navbar navbar-default" role="navigation" id="slide-nav">
			  <div class="container-fluid">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
				  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				  </button>
					<a class="navbar-brand" href="{{ url('home') }}"><img src="{{ asset('front/images/logo.png')}}" alt="logo" class="broken-image"/></a>
				</div>

				<!-- Collect the nav links, forms, and other content for toggling -->
				<div id="slidemenu">
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					  
					  <?php
                                if (isset(Auth::user()->id)) {
                                    ?>
					  <a class="btn btn-primary btn-login fr" href="{{url('auth/logout')}}">LOGOUT</a>
					  <?php }else{ ?>
						 <a class="btn btn-primary btn-login fr" href="{{url('login')}}">LOGIN</a> 
					<?php  } ?>
					  <ul class="nav navbar-nav navbar-main fr">
					  
						<li><a href="{{ url('home') }}">Home</a></li>
						 
						<li class="dropdown submenu-cols">
						  <a href="{{ url('products') }}" onclick="window.location='<?php echo url('products'); ?>'" class="dropdown-toggle" data-toggle="dropdown">Products</a>
						  
						  <?php if(isset($categories) && !empty($categories)) { ?>	
							  
						  <?php><ul class="dropdown-menu">
							  <?php foreach($categories as $key=>$val) { 
								$childs = isset($val['categories'])?$val['categories']:array();
							?>
							<li <?php echo (isset($childs) && !empty($childs))?'class="dropdown"':''; ?>>
							
							  <a href="javascript:void(0);" class="dropdown-toggle" <?php  echo isset($childs) && !empty($childs)?'data-toggle="dropdown"':''; ?> ><?php echo $val['name']?></a>
							  <?php 
							  if(isset($childs) && !empty($childs))
							  { ?>
							  <ul class="dropdown-menu">
								  <?php 
								  
								  foreach($childs as $k=>$v)
								  { 
								  ?>
								<li><a href="<?php echo url('products/category-'.$k); ?>"><?php echo $v; ?></a></li>
								<?php } ?>
								
							  </ul>
							  <?php } ?>
							</li>
							<?php } ?>
							
						  </ul><?php ?>
						<?php } ?>
						
						
						
						
						
						
						</li>
						
						
						
						
						
						
						<?php if(isset($allCategoriesServices) && !empty($allCategoriesServices)) {
							  ?>
						<li class="dropdown">
						  <a href="{{ url('laboratory-services') }}" onclick="window.location='<?php echo url('laboratory-services'); ?>'" class="dropdown-toggle" data-toggle="dropdown">Laboratory Services</a>
						  <?php /*?><ul class="dropdown-menu">
							  <?php foreach($allCategoriesServices as $key=>$val) { ?>
							<li><a href="<?php echo url('laboratory-services/category-'.$val->id); ?>"><?php echo $val->name; ?></a></li>
							<?php } ?>
							
						  </ul><?php */?>
						</li>
						<?php  } ?>
						 
						<li class="dropdown">
						  <a href="#dropdown" class="dropdown-toggle" data-toggle="dropdown">Services</a>
						  <ul class="dropdown-menu">
							<li><a href="#c">Reverse Logistics</a></li>
							<li><a href="#f">Health Professional Services</a></li>
							<li><a href="#e">Strategic Health Analysis and Advisory Services</a></li>
						  </ul>
						</li>
						
						<li><a href="#!5">Contact us</a></li>
						
					  </ul>
					  
					  
						

					</div><!-- /.navbar-collapse -->
				</div>
			  </div><!-- /.container-fluid -->
			</nav>
		</div>
	</section>
</header>
