<?php
use App\Categories;
use App\Functions\Functions; 
$allCategories = Categories::orderBy('parent_id','asc')->get();
$categories = Functions::getCategories($allCategories);

?>
	<div class="prod__left col-sm-3 ">
<?php if(isset($categories) && !empty($categories)) { ?>
		<h3>Medical Products</h3>

		<nav class="navbar navbar-default" role="navigation">
		  <div class="container-fluid p0">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse p0" id="bs-example-navbar-collapse-1">
			
			  <ul class="nav navbar-nav navbar-list navbar-mega">
			  <?php foreach($categories as $key=>$val) { 
				  $childs = isset($val['categories'])?$val['categories']:array();
				  if(isset($childs) && !empty($childs)) 
				  { 
				  ?>
				<li class="dropdown">
				  <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><?php echo $val['name']; ?></a>
				  <?php if(isset($childs) && !empty($childs)) { ?>
						  <ul class="dropdown-menu">
							  <?php foreach($childs as $k=>$v) { ?>
							<li><a href="<?php echo url('products/category-'.$k); ?>"><?php echo $v; ?></a></li>
							<?php } ?>
							
						  </ul>
				<?php } ?>  
				</li>
				<?php }else
				{ ?>
					<li><a href="#"><?php echo $val['name']; ?></a></li>
			<?php	} ?>
			 <?php } ?>


				
			  </ul>
			  
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
	<?php } ?>
	
	</div>

		
