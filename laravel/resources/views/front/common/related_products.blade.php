<?php 
use App\Products;
 $products = Products::where('type', 'simple')->where('showcaseproduct', 1)->limit(7)->get();
if(isset($products) && !empty($products))
{ 
?>
	
	<div class="realatedprod-area mt0 col-sm-12">
	
		<div class="hed lv2">
			<h2>Related Products</h2>
		</div>
	
	
	
			<div class="swiper-container dontfly s1">
				<div class="swiper-wrapper">
					<?php foreach($products as $key=>$val) {
						
						$product_images=array();
						
						if(isset($val->image) && $val->image!='')
						{
							$product_images = explode(',',$val->image);
						}
						 ?>
					<div class="swiper-slide">
					
						<div class="prodbox">
							<div class="prodbox__inr">
								
								<div class="prodbox__img">
									<?php if(isset($product_images) && !empty($product_images)) { ?>
								<img src="{{ asset('uploads/products')}}/<?php echo (isset($product_images[0]) && $product_images[0]!='')?$product_images[0]:'';  ?>" alt="<?php echo (isset($product_images[0]) && $product_images[0]!='')?$product_images[0]:'';  ?>" />
								<?php } ?>
								</div>
								
								<div class="prodbox__cont">
									<h4><?php echo $val->name; ?></h4>
									<div class="cont"><?php echo $val->teaser; ?></div>
									<div class="prod__price">
										<span>₦</span> <?php echo $val->price; ?>
									</div>
									
									<div class="prod__atc">
										<button onclick="window.location='<?php echo url('product/'.$val->id); ?>'" class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</button>							
									</div>
								</div>
								
								<div class="prodbox__actions clrlist listview">
									<ul>
										<!--<li><a href="#"><i class="fa fa-heart"></i></a></li>-->
										<li><a href="<?php echo url('product/'.$val->id); ?>"><i class="fa fa-shopping-cart"></i></a></li>
										<!--<li><a href="#"><i class="fa fa-search"></i></a></li>-->
									</ul>
								</div>
								
							</div>
						</div>
					
					
					</div>
						<?php } ?>
					<!--<div class="swiper-slide">
					
						<div class="prodbox">
							<div class="prodbox__inr">
								
								<div class="prodbox__img">
									<img src="images/product6.jpg" alt="" />
								</div>
								
								<div class="prodbox__cont">
									<h4>Cotrapper Tab</h4>
									<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
									<div class="prod__price">
										<span>₦</span> 15.00
									</div>
									
									<div class="prod__atc">
										<button class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</button>							
									</div>
								</div>
								
								<div class="prodbox__actions clrlist listview">
									<ul>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-search"></i></a></li>
									</ul>
								</div>
								
							</div>
						</div>
					
					
					</div>
					
					
					<div class="swiper-slide">
					
						<div class="prodbox">
							<div class="prodbox__inr">
								
								<div class="prodbox__img">
									<img src="images/product2.jpg" alt="" />
								</div>
								
								<div class="prodbox__cont">
									<h4>Cotrapper Tab</h4>
									<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
									<div class="prod__price">
										<span>₦</span> 15.00
									</div>
									
									<div class="prod__atc">
										<button class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</button>							
									</div>
								</div>
								
								<div class="prodbox__actions clrlist listview">
									<ul>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-search"></i></a></li>
									</ul>
								</div>
								
							</div>
						</div>
					
					
					</div>
					
					
					
					<div class="swiper-slide">
					
						<div class="prodbox">
							<div class="prodbox__inr">
								
								<div class="prodbox__img">
									<img src="images/product3.jpg" alt="" />
								</div>
								
								<div class="prodbox__cont">
									<h4>Cotrapper Tab</h4>
									<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
									<div class="prod__price">
										<span>₦</span> 15.00
									</div>
									
									<div class="prod__atc">
										<button class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</button>							
									</div>
								</div>
								
								<div class="prodbox__actions clrlist listview">
									<ul>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-search"></i></a></li>
									</ul>
								</div>
								
							</div>
						</div>
					
					
					</div>
					
					
					<div class="swiper-slide">
					
						<div class="prodbox">
							<div class="prodbox__inr">
								
								<div class="prodbox__img">
									<img src="images/product4.jpg" alt="" />
								</div>
								
								<div class="prodbox__cont">
									<h4>Cotrapper Tab</h4>
									<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
									<div class="prod__price">
										<span>₦</span> 15.00
									</div>
									
									<div class="prod__atc">
										<button class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</button>							
									</div>
								</div>
								
								<div class="prodbox__actions clrlist listview">
									<ul>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-search"></i></a></li>
									</ul>
								</div>
								
							</div>
						</div>
					
					
					</div>
					
					
					<div class="swiper-slide">
					
						<div class="prodbox">
							<div class="prodbox__inr">
								
								<div class="prodbox__img">
									<img src="images/product3.jpg" alt="" />
								</div>
								
								<div class="prodbox__cont">
									<h4>Cotrapper Tab</h4>
									<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
									<div class="prod__price">
										<span>₦</span> 15.00
									</div>
									
									<div class="prod__atc">
										<button class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</button>							
									</div>
								</div>
								
								<div class="prodbox__actions clrlist listview">
									<ul>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-search"></i></a></li>
									</ul>
								</div>
								
							</div>
						</div>
					
					
					</div>
					
					
					<div class="swiper-slide">
					
						<div class="prodbox">
							<div class="prodbox__inr">
								
								<div class="prodbox__img">
									<img src="images/product5.jpg" alt="" />
								</div>
								
								<div class="prodbox__cont">
									<h4>Cotrapper Tab</h4>
									<div class="cont">Lorem Ipsum is simply dummy text of the printing...</div>
									<div class="prod__price">
										<span>₦</span> 15.00
									</div>
									
									<div class="prod__atc">
										<button class="btn btn-primary btn-atc"><i class="fa fa-shopping-cart"></i> ADD TO CART</button>							
									</div>
								</div>
								
								<div class="prodbox__actions clrlist listview">
									<ul>
										<li><a href="#"><i class="fa fa-heart"></i></a></li>
										<li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
										<li><a href="#"><i class="fa fa-search"></i></a></li>
									</ul>
								</div>
								
							</div>
						</div>
					
					
					</div>
					-->
					
				</div>
				
				<!-- Add Pagination -->
				<div class="swiper-pagination hidden"></div>

				<!-- Add Arrows -->
				<div class="swiper-button-next swiper-button-next1"><i class="fa fa-angle-right"></i></div>
				<div class="swiper-button-prev swiper-button-prev1"><i class="fa fa-angle-left"></i></div>

			</div>
			
	
	
	</div>
	<?php } ?>
	
