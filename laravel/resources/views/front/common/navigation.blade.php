<?php
$cart = Session::get('cart');
?>
<header>
    <section class="hdr-area hdr-nav hdr--sticky blur-nav--hover0 cross-toggle fixed" >
        <div class="container">
            <nav class="navbar navbar-default" role="navigation" id="slide-nav">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a href="{{url('/')}}" class="navbar-brand anime-left" >
                            <img src="{{ asset('front/images/logo.png') }}" alt="Logo" />
                        </a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div id="slidemenu">
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                            <ul class="nav navbar-nav navbar-main">
                                <li><a href="{{url('how-to-order')}}">HOW TO ORDER</a></li>
                                <li class=""><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">TEST MENU<span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="{{url('shop')}}">All Test Details</a></li>
                                        <li><a href="{{url('bundle')}}">Bundle Test</a></li>
                                    </ul>
                                </li>


                                <li><a href="{{url('about-us')}}">ABOUT US</a></li>
                                <li><a href="{{url('locations')}}">LOCATIONS</a></li>
                                <li><a href="{{url('blog')}}">BLOG</a></li>


                                <?php
                                if (isset(Auth::user()->id)) {
                                    ?>




                                    <li class=""><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account<span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url('myorders')}}" class="pagelinkcolor">My Orders</a></li>
                                            <li><a href="{{ url('changepassword')}}" class="pagelinkcolor">Change Password</a></li>
                                            <li><a href="{{ url('profile')}}" class="pagelinkcolor">Profile</a></li>
                                            <li><a href="{{ url('auth/logout')}}" class="pagelinkcolor">Log Out</a></li>
                                        </ul>
                                    </li>


                                    <?php
                                } else {
                                    ?>
                                    <li class="login-link anime-right"><a href="{{url('login')}}">Login</a></li>

                                    <li class="KNav6"><a href="{{url('connect')}}">MD CONNECT &reg;</a></li>
                                    <?php
                                }
                                ?>


                                <li class="cart-item">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-shopping-cart fa-2"></i> <span>Cart<sup class="badge"><?php // echo count($cart);     ?></sup></span> </a>

                                    <ul class="dropdown-menu">
                                        <?php if (count($cart) > 0) { ?>
                                            <?php
                                            foreach ($cart as $product) {
                                                if ($product->type == 'additional') {
                                                    continue;
                                                }
                                                ?>
                                                <li><a href="{{url('product/')}}/<?php echo $product->key ?>" class="pagelinkcolor"><?php echo $product->product_name ?></a></li> 
                                            <?php } ?>
                                            <li>
                                                <div class="p10 col-sm-12"><a class="btn btn-warning" href="{{url('cart/view')}}" class="pagelinkcolor"><i class="fa fa-arrow-right"></i> View Cart</a>

                                                    <a class="btn btn-primary" href="{{url('checkout')}}" class="pagelinkcolor"><i class="fa fa-shopping-cart"></i> Check out</a></div>
                                            </li>

                                        <?php } else { ?>
                                            <li><a href="javascript:void(0);" class="pagelinkcolor">No items</a></li> 
                                            <?php } ?>
                                    </ul>
                                </li>

                            </ul>
                        </div><!-- /.navbar-collapse -->
                    </div>
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </section>
</header>