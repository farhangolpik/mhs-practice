<section class="page-banner col-sm-12 bg-cvr dark-area" style="background-image:url('<?php echo asset('front/images/product-bg.jpg'); ?>');">
	
	<div class="hed underline--plus">
		<h2><?php echo (isset($breadcrumb['banner_title']) && $breadcrumb['banner_title']!='')?$breadcrumb['banner_title']:''; ?></h2>
	</div>
	
	<ol class="breadcrumb">
	  <li><a href="<?php echo (isset($breadcrumb['b1_link']) && $breadcrumb['b1_link']!='')?$breadcrumb['b1_link']:url('home'); ?>"><?php echo (isset($breadcrumb['b1']) && $breadcrumb['b1']!='')?$breadcrumb['b1']:'Home'; ?></a></li>
	  <?php if(isset($breadcrumb['b2']) && $breadcrumb['b2']!='') { ?><li class="active"><?php echo (isset($breadcrumb['b2']) && $breadcrumb['b2']!='')?$breadcrumb['b2']:''; ?></li><?php } else{ echo ''; }?>
	  <?php if(isset($breadcrumb['b3']) && $breadcrumb['b3']!='') { ?><li class="active"><?php echo $breadcrumb['b3']; ?></li> <?php } else{ echo ''; }?>
	</ol>
	
</section>
