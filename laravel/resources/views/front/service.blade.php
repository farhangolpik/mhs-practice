@extends('front')

@section('content')

@include('front/common/page_banner')

	<section class="services-detail--section col-sm-12 bg-white last-section pt60 pb30">
		<div class="services-detail--cont col-sm-7 fs16">
			<h2>DESCRIPTION</h2>
			
			<div class="cont "><?php echo isset($product->description)?$product->description:''; ?></div>
	
        <div class="srvdtl__list clrlist listview mt20">
			<ul>
				<li><strong>Fasting Required:</strong> No</li>
				<li><strong>Preferred Specimen:</strong> Haematology</li>
				<li><strong>Reference Range(s):</strong> 5-30 mcg/L</li>
				<li><strong>Turnaround Time:</strong> 3-5 days</li>
				<li><strong>Test Code:</strong> 2456</li>
			</ul>
      </div>		
				
		</div>
		
		<div class="srvdtl__oval col-sm-5 clrlist listview">
		
			<h3><?php echo isset($product->name)?ucfirst($product->name):''; ?></h3>
			
			<h2><sup>₦</sup><?php echo isset($product->price)?ucfirst($product->price):'0.00'; ?></h2>
			
			<div class="cont"><?php echo isset($product->teaser)?ucfirst($product->teaser):''; ?></div>
			
			<button id="btn_add_to_cart" type="button" class="btn btn-default btn-md">CHECKOUT</button>
			<form id="form_add_to_cart" class="prod-detail-form">
				<input type="hidden" value="1" name="quantity" id="quantity"  />
				<input type="hidden" name="total_price"  id="total_price" value="<?php echo isset($product->price)?ucfirst($product->price):'0.00'; ?>" />
				<input type="hidden" name="price"  id="price" value="<?php echo isset($product->price)?ucfirst($product->price):'0.00'; ?>" />
				<input type="hidden" name="product_id"  id="product_id" value="<?php echo isset($product->id)?ucfirst($product->id):0; ?>" />
			</form>
		</div>
		
		<script>

    $(document).ready(function () {
        $("#btn_add_to_cart").click(function () {

            var price = $('#total_price').val();
            var form = $('#form_add_to_cart').serialize();

            //    return false;
            var jqxhr = $.get('<?php echo url('cart/add'); ?>', form, function () {
                // alert( "Product added to cart." );
                
                window.location = '<?php echo url('cart/view'); ?>';

            })
                    .done(function () {
                        //alert( "second success" );
                    })
                    .fail(function () {
                        //alert( "error" );
                    })
                    .always(function () {
                        //alert( "finished" );
                    });
        });


    });
</script>
	</section>



@endsection
