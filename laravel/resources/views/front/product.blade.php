@extends('front')

@section('content')

@include('front/common/page_banner')
<section class="products-page proddtl-area last-section bg-white">
	<?php 
		$prod_images = isset($product->image)?$product->image:'';
		if(isset($prod_images) && $prod_images!='')
		{
			$prod_images_array = explode(',',$prod_images);
		}
		else
		{
			$prod_images_array = array();
		}

	?>
	<div class="row">
	<?php if(isset($prod_images_array) && !empty($prod_images_array)) { ?>
	<div class="proddtl__img col-sm-6 thumb-indicat">
	
	<div id="prodSlider" class="carousel slide" data-ride="carousel">
	  <ol class="carousel-indicators">
		 <?php foreach($prod_images_array as $key=>$val) { ?>
		<li data-target="#prodSlider" data-slide-to="<?php echo $key; ?>" class="<?php echo (isset($key) && $key==0)?'active':''; ?>">
			<img src="{{ asset('uploads/products/')}}/<?php echo $val; ?>" alt="<?php echo isset($product->name)?$product->name:''; ?>" />
		</li>
		<?php } ?>
		
	  </ol>

	  
	  <div class="carousel-inner">
		   <?php foreach($prod_images_array as $key=>$val) { ?>
		<div class="item bg-cntr <?php echo (isset($key) && $key==0)?'active':''; ?>" style="background-image:url('{{ asset('uploads/products/')}}/<?php echo $val; ?>');">
		</div>
		<?php } ?>
		<div class="item bg-cntr" style="background-image:url('images/prod-dtl1.png');">
		</div>
		<div class="item bg-cntr" style="background-image:url('images/prod-dtl1.png');">
		</div>
		<div class="item bg-cntr" style="background-image:url('images/prod-dtl1.png');">
		</div>
	  </div>
 
	  <a class="left carousel-control hidden" href="#prodSlider" data-slide="prev">
		<span class="glyphicon glyphicon-chevron-left"></span>
	  </a>
	  <a class="right carousel-control hidden" href="#prodSlider" data-slide="next">
		<span class="glyphicon glyphicon-chevron-right"></span>
	  </a>
	</div>


	</div>
	<?php } ?>
	
	
	
	
	
	
	
	<div class="proddtl__cont col-sm-6">
	
		<h3><?php echo isset($product->name)?$product->name:''; ?></h3>
		
		<!--<div class="rating-area">
			
			<span class="star-icon star-4">
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
                <i class="fa fa-star"></i>
				<i class="fa fa-star"></i>
            </span>
									
			<span>1 Review(s) | Write Your Review</span>
		</div>
		-->
		
		<!--<div class="instock"><small>Availability</small><strong>(In stock)</strong></div>-->
		
		
		<div class="prod__price">
			<span>₦</span> <?php echo isset($product->price)?$product->price:''; ?>
		</div>
		
		<div class="cont">
		<?php echo isset($product->teaser)?$product->teaser:''; ?>
		</div>
		
		
		<div class="proddtl__actions clrlist">
				
		<ul>
			<li>
				<div class="form-group input-number-area">
					<div class="input-group">
					<span class="input-group-addon qty">Qty</span>
						<input class="form-control input-number has-value" type="number" value="1" min="0" max="10">
						<span class="input-group-addon">
							<span class="form-control input-number-decrement">-</span>
							<span class="form-control input-number-increment">+</span>
							
						</span>
					</div>
				</div>
			</li>
			<li>
				<button class="btn btn-primary">Add to cart</button>
			</li>
			<!--<li>
				<button class="btn"><i class="fa fa-heart"></i></button>
			</li>
			<li>
				<button class="btn"><i class="fa fa-bar-chart "></i></button>
			</li>-->
		</ul>
						
		</div>			
	
	</div>
	
	</div>
	
	<div class="clearfix"></div>
	
	<div class="proddtl__desc mt30">
		<ul class="nav nav-tabs">
		  <li class="active"><a href="#proddtltab1" data-toggle="tab">Details</a></li>
		  <li><a href="#proddtltab2" data-toggle="tab">More Information</a></li>
		  <!--<li><a href="#proddtltab3" data-toggle="tab">Reviews</a></li> -->
		</ul>


		<div class="tab-content p15">
		  <div class="tab-pane active" id="proddtltab1">
		  <?php echo isset($product->description)?$product->description:''; ?>
		  </div>
		  <div class="tab-pane" id="proddtltab2">
		  <?php echo isset($product->requirments)?$product->requirments:''; ?>
		  </div>
		  <!--<div class="tab-pane" id="proddtltab3">
		  ibution of letters, as opposed to using 'Content here, content here', making it look like readable English. have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).It is a long established fact that a reader will be distracted by the readable content of a page when looking at its</div>
		</div>-->
	</div>

				
	<?php
				
		//include("related-products.php");
				
	?>
				
</section>


@endsection
