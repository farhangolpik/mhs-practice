@extends('front')

@section('content')

@include('front/common/page_banner')
<?php 
use App\Functions\Functions;
?>


<section class="table-area col-sm-12 bg-white pt60">
	  <div class="container">
		  <div class="pagi-area col-sm-4 text-right">
					<?php 
						$tempPageurl =   $_SERVER['REQUEST_URI'];
						if (strpos($tempPageurl, 'screen-') !== false) {
						$screen_array = explode("/screen-", $tempPageurl);
						$page_url = $screen_array[0];
						}   
						else
						{
						$page_url = $tempPageurl;
						} 

						$querystring=$page_url; 

						echo Functions::pagination($result_count, $per_page,$page_offset, $querystring,'/screen-');
					?>
					<!--<ul class="pagination">
					  <li class="active"><a href="#">1</a></li>
					  <li><a href="#">2</a></li>
					  <li><a href="#">3</a></li>
					  <li><a href="#">&raquo;</a></li>
					</ul>-->
				</div>
		 <?php 
		 if(isset($products) && !empty($products)) { 
				
		 ?>
		<table class="table table-bordered table-valign0">
			<thead>
			  <tr>
				<th class="col-sm-3">Test Codes</th>
				<th class="col-sm-9">Laboratory Services</th>
			  </tr>
			</thead>

			<tbody>
			<?php 
			foreach($products as $key=>$product_alpha) { 
				if(isset($product_alpha) && !empty($product_alpha))
				{
					foreach($product_alpha as $k=>$val)
					{
						/*$product_images=array();
						
						if(isset($val->images) && $val->images!='')
						{
							$product_images = explode(',',$val->images);
						}*/
			?>
			 <tr>
				<td><?php echo $val->sku; ?></td>
				<td><span><?php echo $val->name; ?></span> <a href="<?php echo url('laboratory-service/'.$val->id); ?>" class="btn btn-primary fr">VIEW</a></td>
			</tr>
			<?php } } } ?>
			
			
			
           </tbody>
		  </table>
		  
		  <?php } ?>
		  
		</div>
	</section>



@endsection
