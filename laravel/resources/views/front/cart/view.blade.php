@extends('front')

@section('content')

@include('front/common/page_banner')
<?php
$currency = Config::get('params.currency');
$required = "required";

list($year, $month, $date) = isset($user->dob)?explode('-', $user->dob):array('','','');
?>
<script src="https://www.2checkout.com/checkout/api/2co.min.js"></script>
	  <section class="checkout-section col-sm-12 bg-white pt60 gray-inputs">
		<div class="container">
		
			@if (count($errors->checkout) > 0)
        <span><div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->checkout->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div></span>
        @endif
        
			<div class="alert   checkout-top">
				@if ($countCart==0)<span>
                    <div class="alert alert-success">
                        <h4><i class="icon fa fa-check"></i> &nbsp  Your Basket is empty</h4>
                    </div></span>
            @endif
			 
			 
			   
			   <?php  if(isset($userTmp) && $userTmp>0) { echo ''; }else{ ?>
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			    <span class="cross">Returning Customer<a href="<?php echo url('login');?>"> Click here to login</a></span>
			   <?php } ?>
			  
		    </div>
			
			<!--<div class="alert   checkout-top">
			   <span class="cross">Have a Coupon??<a href=""> Click here to enter your Code</a></span>
			   
			   <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			   
		    </div>-->
			
		<form id="checkout_cart_form" class="billing__form" action="{{ url('postOrder') }}" method="post" enctype="multipart/form-data" >
			<input name="token" type="hidden" value="" />
			<input name="_token" type="hidden" value="{{ csrf_token() }}" />
			<div class="checkout-billing--form fom col-sm-6 placeholder--hover pl0">
			   <h3 class="mb20">PICKING DETAILS</h3>
			   
			   
			   <div class="upload-preview col-sm-12 mb30 p0" id="uploadPreview" style="display:none;" >
					<h5>Image preview upload*</h5>
					<div class="upload-preview__img col-sm-12 p0">
					<div class="upload-preview--inr">
						<img class="image" src="{{ asset('front/images/profile-image.png') }}" alt="" />
					  </div>
					  </div>
					
				  <input name="user_picture" type="file" class="file-input col-sm-12 p0" />
				  
				  <div class="upload-preview--cont mt10 col-sm-12 p0">
				  <p class="mb0">Acceptable file format:JPG,JPEG & PNG </p>
				  <p>File size of ht e image should not exceed 2MB</p>
				  </div>
				  
				</div>
			   
			  
			
				<div class="form-group col-sm-6 pl0">
					 <input type="text" class="form-control" value="<?php  if(isset($user->firstName)){ echo $user->firstName; }elseif(old('firstName')){ echo old('firstName'); }else{ echo ''; } ?>" name="firstName" placeholder="First Name*" required/>
		        </div>
				
				<div class="form-group col-sm-6 pr0">
					 <input type="text" class="form-control" value="<?php if(isset($user->lastName)){ echo $user->lastName; }elseif(old('lastName')){ echo old('lastName'); }else{ echo ''; } ?>" name="lastName" placeholder="Last Name*" required/>
		        </div>
				<div id="servicenumber" style="display:none;" >
					
				</div>
				<?php  if(isset($countries) && !empty($countries)) { ?>
				<div class="form-group">
					<select id="country_id"   name="country_id" required class="form-control">
						<option value="" disabled selected hidden>Select Country</option>
						<?php foreach($countries as $key=>$val) { ?>
						<option <?php if(isset($address->country) && $address->country==$key){ echo 'SELECTED'; }elseif(old('country_id') && old('country_id')==$key){ echo 'SELECTED'; }else{ echo ''; }?> value="<?php echo $key; ?>" ><?php echo $val; ?></option>
						<?php } ?>
						
					</select>
				</div>
				<?php } ?>
				<div class="form-group">
					 <input type="text" class="form-control"  value="<?php if(isset($address->address)){ echo $address->address; }elseif(old('address')){ echo old('address'); }else{ echo ''; } ?>" name="address" placeholder="Street Address" required/>
		        </div>
				
				<div class="form-group">
					 <input type="text" class="form-control" value="<?php if(isset($address->address2)){ echo $address->address2; }elseif(old('address2')){ echo old('address2'); }else{ echo ''; } ?>" name="address2" placeholder="Appartment,suite, unit etc (optional)" />
		        </div>
				
				<div class="form-group col-sm-6 pl0">
					 <input type="text" class="form-control" value="<?php if(isset($address->state)){ echo $address->state; }elseif(old('state')){ echo old('state'); }else{ echo ''; } ?>" name="state" placeholder="State*" required/>
		        </div>
				
				<div class="form-group col-sm-6 pr0">
					 <input type="text" class="form-control" value="<?php if(isset($address->city)){ echo $address->city; }elseif(old('city')){ echo old('city'); }else{ echo ''; }  ?>" name="city" placeholder="City*" required/>
		        </div>
				
				<div class="form-group col-sm-6 pl0">
					 <input type="text" class="form-control" value="<?php if(isset($user->email)){ echo $user->email; }elseif(old('email')){ echo old('email'); }else{ echo ''; } ?>" name="email" placeholder="Email*" required/>
		        </div>
				
				<div class="form-group col-sm-6 pr0">
					 <input type="text" class="form-control" value="<?php if(isset($address->phone)){ echo $address->phone; }elseif(old('phone')){ echo old('phone'); }else{ echo ''; } ?>" name="phone" placeholder="Phone*" required/>
		        </div>
				<div class="form-group col-sm-6 pr0">
					 <input  class="form-control"  value="<?php  if(old('ccNo')){ echo old('ccNo'); }else{ echo ''; } ?>"  placeholder="Card Number*" id="ccNo" name="ccNo" type="text" size="20" value="" autocomplete="off" required />
		        </div>
				<div class="form-group col-sm-6 pr0">
					 <input  class="form-control"  value="<?php  if(old('expMonth')){ echo old('expMonth'); }else{ echo ''; } ?>"  placeholder="Expiry Month size 2 *" type="text" size="2" id="expMonth" name="expMonth" required />
		        </div>
				<div class="form-group col-sm-6 pr0">
					 <input  class="form-control"   value="<?php  if(old('expYear')){ echo old('expYear'); }else{ echo ''; } ?>"  placeholder="Expiry Year size 2 *" type="text" size="2" id="expYear" name="expYear" required  />
		        </div>
				<div class="form-group col-sm-6 pr0">
					 <input  class="form-control"   value="<?php  if(old('cvv')){ echo old('cvv'); }else{ echo ''; } ?>" placeholder="Cvv code *" id="cvv" name="cvv" size="4" type="text" value="" autocomplete="off" required  />
		        </div>
				
				
				<span id="errorcard" style="display:none;" ><div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>

					<li id="error_li" ></li>

					</ul>
					</div>
				</span>
            
				 <?php   if(isset($userTmp) && $userTmp>0) { echo ''; }else{ ?>
				<div class="form-group checker-area">
					 <span class="fnc__checkbox"><input name="account_create_check" type="checkbox" class="form-control"/></span> <label>Create an account?</label>
				 </div>
			   <?php } ?>
			   
				

			  
		   </div>
		   
		   
		   <div class="checkout-billing--form fom col-sm-6 placeholder--hover pr0">

			   <h3>SHIP TO A DIFFERENT ADDRESS</h3>
			   
			 <!--  <div class="form-group hidden-checkbox pul-rgt">
					<input type="checkbox" id="toggle"/>
					<div class="checkylbl">
					  <label for="toggle"></label>
					</div>
				</div>
				-->
				
			    
		   
			<div class="form-group"> 
		       <textarea id="message2" name="message" class="form-control" rows="6" placeholder="Notes about your order, e.g special notes for delivery." required></textarea>
		     </div>
			
		  </div>
		   
	  <?php  if(isset($cart) && !empty($cart)) { ?>
	  <div class="yourorder-area col-sm-12 p0 table-cart--total">
		<h3>YOUR ORDER</h3>
		
		<table class="table table-bordered">
			<thead>
			  <tr>
				<th class="w50">LAB TEST</th>
				<th class="w50">TOTAL</th>
			  </tr>
			</thead>

			<tbody>
			<?php 
			$strservices=''; 
			$contain_services=0;
			$sum = 0; 
			foreach($cart as $key=>$product) {  ?>
			<?php
				$rowTotal = $product->total_price * $product->quantity;
				$sum += $rowTotal;
				if(isset($product->type) && $product->type == 'service')
				{
					$type_product = 'LAB TEST';
					$contain_services=1;
					$strservices.='<div class="form-group">';
					$strservices.=	 '<input type="text" class="form-control" value="'.$product->product_sku.'" name="service_ids[]" placeholder="Your ID Number" required/>';
					$strservices.='</div>';
				}
				elseif(isset($product->type) && $product->type == 'simple')
				{
					$type_product = 'PRODUCT';
				}
				else
				{
					$type_product = '';
				}
			?>
			 <tr>
				<td><?php echo isset($product->product_name)?ucfirst($product->product_name.'  ('.$type_product.')'):''; ?><!--<strong>x 1</strong>--><button onclick="deleteCart('<?php echo $product->cart_id ?>');" class="btn-xs btn-danger" type="button">remove</button></td>
				<td><?php echo Config::get('params.currency.USD.symbol'); ?> <?php echo isset($product->product_price)?ucfirst($product->product_price):''; ?></td>
			</tr>
			<?php } ?>
			
			
			<tr>
				<td><strong>Total</strong></td>
				<td><mark><?php echo Config::get('params.currency.USD.symbol'); ?> <?php echo $sum ?></mark></td>
			</tr>
			
           </tbody>
		  </table>
		  
		</div>
	 <?php } ?>
	 
	 
	 
	 
	 
        <div class="checkout-placeorder--form fom col-sm-12 placeholder--hover bg-smoke p15">

	  
		   
				<!--<div class="form-group">
				<input type="radio" class="form-control" name="pay"/>Check payments</label>
				</div>

				<div class="check-payment--box">
				<p>Please send a check to Store Name, Store Street, Store Town, Store State / County, Store Postcode.</p>
				</div>

				<div class="form-group has__img">
				<div class="input-group clr-addon">
				<span class="input-group-addon">
				<input type="radio" class="form-control" name="pay" /></span>
				<label>PayPal <img src="{{ asset('front/images/paypal.png') }}"/></label></span>
				</div>
				</div>-->
				 <input type="hidden" name="grandTotal" value="<?php echo isset($sum)?$sum:0;  ?>"/>
				 <button type="button" class="btn btn--loading btn-primary btn-md pul-rgt" id="load" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Processing">PLACE ORDER</button>
				
		  
		  </div>
 </form>
     </section>



					<script>
						
						// A $( document ).ready() block.
$( document ).ready(function() {
    var contain_services = '<?php echo $contain_services; ?>'
    var strservices = '<?php echo  $strservices; ?>'; 
    if(contain_services>0)
    {
		$('#uploadPreview').css('display','block');
		$('#servicenumber').css('display','block'); 
		if(strservices!='')
		{
			$('#servicenumber').html(strservices);
		}
	}
});

$( "#load" ).click(function() {
  $('#checkout_cart_form').submit();
});

function deleteCart(id) {

            window.top.location = "<?php echo url('cart/delete');?>/" + id;
        }
						jQuery('[type=file]').change(function(){
							var curElement = jQuery(this).parent().parent().find('.upload-preview__img img');
							console.log(curElement);
							var reader = new FileReader();
							reader.onload = function (e) {
							// get loaded data and render thumbnail.
							curElement.attr('src', e.target.result);
							};
							// read the image file as a data URL.
							reader.readAsDataURL(this.files[0]);
						});
						
						
						
						
						
						
						
						
						// Called when token created successfully.
    var successCallback = function(data) {
        var myForm = document.getElementById('checkout_cart_form');

        // Set the token as the value for the token input
        myForm.token.value = data.response.token.token;

        // IMPORTANT: Here we call `submit()` on the form element directly instead of using jQuery to prevent and infinite token request loop.
        myForm.submit();
    };
$('#errorcard').css('display','none');
    // Called when token creation fails.
    var errorCallback = function(data) {
        if (data.errorCode === 200) {
            tokenRequest();
            
        } else {
			$('#errorcard').css('display','block');
			$('#error_li').html(data.errorMsg);
           // alert(data.errorMsg);
        }
    };

    var tokenRequest = function() {
        // Setup token request arguments
        var args = {
            sellerId: "901358688",
            publishableKey: "32BE9C00-D97F-4264-A9CE-A665079F8243",
            ccNo: $("#ccNo").val(),
            cvv: $("#cvv").val(),
            expMonth: $("#expMonth").val(),
            expYear: $("#expYear").val()
        };

        // Make the token request
        TCO.requestToken(successCallback, errorCallback, args);
    };

    $(function() {
        // Pull in the public encryption key for our environment
        TCO.loadPubKey('sandbox');

        $("#checkout_cart_form").submit(function(e) {
            // Call our token request function
            tokenRequest();

            // Prevent form from submitting
            return false;
        });
    });
					</script>




@endsection
