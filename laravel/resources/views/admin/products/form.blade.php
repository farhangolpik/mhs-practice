<?php
$required = "required";
?>
@include('admin/commons/errors')
<div class="form-group">
    {!! Form::label('name') !!}
    {!! Form::text('name', null , array('class' => 'form-control',$required) ) !!}
</div>
<?php if(isset($type) && $type==='service') {?>
<div class="form-group">
    {!! Form::label('code') !!}
    {!! Form::text('sku', null , array('class' => 'form-control',$required) ) !!}
</div>
<?php } ?>


<div class="box box-primary">
    <div class="box-header with-border">
        <h3 class="box-title"> {!! Form::label('categories') !!} </h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>

        </div>
    </div>
    <div class="box-body">
        <div class="form-group">
            <ul class="products-list product-list-in-box" id="tree1">
                <?php
                $i = 1;
                foreach ($categories as $id => $category) {
                    ?>
                    <div class="col-md-4">
                        <li><input type="checkbox" class="checkall" id="<?php echo $id; ?>"/><?php echo '-' . $category['name']; ?>

                            <ul>
                                <?php
                                if (!empty($category['categories'])) {

                                    foreach ($category['categories'] as $subCatId => $subCategory) {
                                        $check = false;

                                        if (!empty($productsCategories) && in_array($subCatId, $productsCategories)) {
                                            $check = true;
                                        }
                                        ?>
                                        <li>{!! Form::checkbox('categories[]', $subCatId,$check,array('class'=>'sub_cat_'.$id)) !!} 
                                            {!! Form::label('sub_cat_'.$subCatId, $subCategory) !!}</li>
                                        <?php
                                    }
                                } else {
                                    echo "<li>no categories</li>";
                                }
                                ?>
                            </ul></li> </div>  
                    <?php
                    if ($i == 3) {
                        echo "<br clear='all'/>";
                        $i = 0;
                    }
                    $i++;
                }
                ?>
            </ul>
        </div>
    </div>
</div>
<br clear='all'/><br clear='all'/>
<div class="form-group">
    {!! Form::label('Short Description') !!}
    {!! Form::text('teaser', null , array('class' => 'form-control') ) !!}
</div>

<div class="form-group">
    {!! Form::label('description') !!}
    {!! Form::textarea('description', null, ['size' => '105x3','class' => 'form-control ckeditor',$required]) !!} 
</div>

<div class="form-group">
    {!! Form::label('requirments') !!}
    {!! Form::textarea('requirments', null, ['size' => '105x3','class' => 'form-control ckeditor']) !!} 
</div>

<div class="form-group">
    {!! Form::label('price') !!}
    {!! Form::text('price', null , array('class' => 'form-control',$required) ) !!}
</div>



<div class="form-group">
    {!! Form::checkbox('sale',1,false,['id'=>'sale']); !!}
    Sale This Product. 
</div>
<?php if(isset($type) && $type==='simple') {?>
<div class="form-group">
    {!! Form::checkbox('homepageproduct',1,false,['id'=>'homepageproduct']); !!}
    Display This Product on Home page. 
</div>
<div class="form-group">
    {!! Form::checkbox('showcaseproduct',1,false,['id'=>'showcaseproduct']); !!}
    Display This Product in related products. 
</div>
<?php } ?>


<div class="form-group">
    {!! Form::label('keywords') !!}
    {!! Form::text('keywords', null , array('class' => 'form-control',$required) ) !!}
</div>
	<ul class="products-list product-list-in-box">
		<li class="item">
			<?php if(isset($product->image) && $product->image!='')
			{
			$pro_images = explode(',',$product->image);
			if(isset($pro_images) && !empty($pro_images))
			{
			foreach($pro_images as $val)
			{
				if($val=='Array')
					continue;
			?>
			<div class="product-img">
			<img src="{{ asset('uploads/products/thumbnail')}}/<?php echo $val; ?>" alt="<?php echo $val; ?>" />
			<br />

			</div>
			<?php }
			}
			} ?>
		</li>
	</ul>
<div class="form-group">
    {!! Form::label('images limit 4 (jpeg,bmp,png,gif) max 10MB') !!}
    {!! Form::file('image[]', ['multiple' => 'multiple'],array($required,'class'=>'form-control')) !!}
</div>
<div class="form-group">
    <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
</div>

<script>
	$("input[type='submit']").click(function(){
		alert('');
        var $fileUpload = $("input[type='file']");
        if (parseInt($fileUpload.get(0).files.length)>4){
         alert("You can only upload a maximum of 2 files");
         return false;
        }
    }); 
    $(document).ready(function () {
        $('.checkall').on('change', function () {
            $(".sub_cat_" + this.id).prop('checked', $(this).prop('checked'));
        });
    });
</script>
