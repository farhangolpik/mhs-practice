@extends('admin/admin_template')

@section('content')
<?php
$currency = Config::get('params.currency');
$orderPrefix = Config::get('params.order_prefix');

?>

<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">


            <div class="box-body">
                <div class="col-lg-6 col-md-6 col-md-6">
                    
                    <div class="box-header with-border">
                        <h3 class="box-title"> Customer's Information</h3>


                    </div>
                    <div class="row">
                        <ul>
                            <li>First Name :{{ isset($data[0]->firstName)?$data[0]->firstName:'' }}</li>
                            <li>Middle Name :{{ isset($data[0]->middleName)?$data[0]->middleName:'' }}</li>
                            <li>Last Name :{{ isset($data[0]->lastName)?$data[0]->lastName:'' }}</li>
                            <li>Date of Birth :{{ isset($data[0]->dob)?$data[0]->dob:'' }}</li>
                            <li>Email: {{ isset($data[0]->email)?$data[0]->email:'' }}</li>
                            <li>Joins:{{ isset($data[0]->created_at)?$data[0]->created_at:'' }}</li>

                        </ul>
                    </div>
                </div>


                <div class="col-lg-6 col-md-6 col-md-6">
                    
                    <div class="box-header with-border">
                        <h3 class="box-title"> Address Information</h3>
                        


                    </div>
                    <div class="row">
                        <ul>
                            <li>Address :{{ isset($data[0]->address)?$data[0]->address:'' }}</li>
                            <li>Address 2:{{ isset($data[0]->address2)?$data[0]->address2:'' }}</li>
                            <li>Phone :{{ isset($data[0]->phone)?$data[0]->phone:'' }}</li>
                            <li>Mobile :{{ isset($data[0]->mobile)?$data[0]->mobile:'' }}</li>
                            <li>Country: {{ isset($data[0]->country)?$data[0]->country:'' }}</li>
                            <li>State: {{ isset($data[0]->state)?$data[0]->state:'' }}  </li>
                            <li>City: {{ isset($data[0]->city)?$data[0]->city:'' }}</li>
                            <li>Zip Code: {{ isset($data[0]->zip)?$data[0]->zip:'' }}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <!-- PRODUCT LIST -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">( Total Orders : {{ isset($orders)?count($orders):0 }} ) </h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>

                </div>
            </div>
            <!-- /.box-header -->
            <?php if (isset($orders) && !empty($orders)) { ?>
                <div class="box-body">
                    <ul class="products-list product-list-in-box">

                        <table class="table" id="order_table">
                            <thead>
                                <tr>
                                    <td>Order Id</td>
                                    <td>Name</td>
                                    <td>Added</td>
                                    <td>Status</td>
                                    <td>Total</td>
                                    <td></td>                                    
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($orders as $order)
                                <tr>
                                    <td><?php //echo $orderPrefix; ?>{{$order->id}}</td>
                                    <td>{{ $order->firstName.' '.$order->lastName }}</td>
                                    <td>{{ date('d M Y',strtotime($order->created_at))}}</td>
                                    <td>{{ ucfirst($order->orderStatus)}}</td>
                                    <td>{{ $currency[Config::get('params.currency_default')]['symbol']}}{{ $order->grandTotal}}</td>
                                    <td><a href="{{ url('admin/order')}}/{{$order->id}}">Detail</a></td>                                    
                                </tr>
                                @endforeach 
                            </tbody>

                        </table>
                        <div id="warning_message" ></div>
                        <div class="loader" style="display: none;">Loading...</div>
                    </ul>
                </div>
            <?php } ?>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <a href="javascript::;" class="uppercase"></a>
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
    <!-- /.col -->

</div>



@endsection
